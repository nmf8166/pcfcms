<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;

use think\facade\Db;
use think\facade\Request;
use think\facade\Cache;

// 栏目列表
class TagChannel extends Base
{
    public $tid = '';
    public $currentstyle = '';
    
    //初始化
    protected function _initialize()
    {
        parent::_initialize();
        $this->tid = input("param.tid/s", ''); // 应用于栏目列表
        // 应用于文档列表
        $aid = input('param.aid/d', 0);
        if ($aid > 0) {
            $cacheKey = 'tagChannel_'.strtolower('home_'.Request::controller().'_'.Request::action());
            $cacheKey .= "_{$aid}";
            $this->tid = cache::get($cacheKey);
            if ($this->tid == false) {
                $this->tid = Db::name('archives')->where('aid', $aid)->value('typeid');
                cache::set($cacheKey, $this->tid);
            }
        }
        // tid为目录名称的情况下
        $this->tid = $this->getTrueTypeid($this->tid);
    }

    /**
     * 获取指定级别的栏目列表
     * @param string type son表示下一级栏目,self表示同级栏目,top顶级栏目
     * @param boolean $self 包括自己本身
     */
    public function getChannel($typeid = '', $type = 'top', $currentstyle = '', $notypeid = '')
    {
        $ChannelType = new \app\common\model\ChannelType;
        $this->currentstyle = $currentstyle;
        $typeid  = !empty($typeid) ? $typeid : $this->tid;
        if (empty($typeid)) {
            // 应用于没有指定tid的列表，默认获取该控制器下的第一级栏目ID
            $controller_name = Request::controller();
            $channeltype_info = $ChannelType->getInfoByWhere(array('ctl_name'=>$controller_name), 'id');
            $channeltype = $channeltype_info['id'];
            $map = array(
                'channeltype'   => $channeltype,
                'parent_id' => 0,
                'is_hidden' => 0,
                'status'    => 1,
            );
            $typeid = Db::name('arctype')->where($map)->order('sort_order asc')->limit(1)->value('id');
        }
        $result = $this->getSwitchType($typeid, $type, $notypeid);
        return $result;
    }

    /**
     * 获取指定级别的栏目列表
     * @param string type son表示下一级栏目,self表示同级栏目,top顶级栏目
     * @param boolean $self 包括自己本身
     */
    public function getSwitchType($typeid = '', $type = 'top', $notypeid = '')
    {
        $pcfglobal = get_global();
        $Channelfield = new \app\common\model\Channelfield;
        $result = array();
        switch ($type) {
            case 'son': // 下级栏目
                $result = $this->getSon($typeid, false);
                break;
            case 'self': // 同级栏目
                $result = $this->getSelf($typeid);
                break;
            case 'top': // 顶级栏目
                $result = $this->getTop($notypeid);
                break;
            case 'sonself': // 下级、同级栏目
                $result = $this->getSon($typeid, true);
                break;
            case 'first': // 第一级栏目
                $result = $this->getFirst($typeid);
                break;
        }
        // 处理自定义表字段的值
        if (!empty($result)) {
            $map = array('channel_id' => $pcfglobal['arctype_channel_id'],);
            $fieldInfo = $Channelfield->getListByWhere($map, '*', 'name');
            $fieldLogic = new \app\home\logic\FieldLogic;
            foreach ($result as $key => $val) {
                if (!empty($val)) {
                    $val = $fieldLogic->handleAddonFieldList($val, $fieldInfo);
                    $result[$key] = $val;
                }
            }
        }
        return $result;
    }

    /**
     * 获取下一级栏目
     * @param string $self true表示没有子栏目时，获取同级栏目
     */
    public function getSon($typeid, $self = false)
    {
        $pcfglobal = get_global();
        $ChannelType = new \app\common\model\ChannelType;
        $result = array();
        if (empty($typeid)) {
            return $result;
        }
        $arctype_max_level = intval($pcfglobal['arctype_max_level']); // 栏目最多级别
        // 获取所有显示且有效的栏目列表
        $map = array(
            'c.is_hidden'   => 0,
            'c.status'  => 1,
            'c.is_del'    => 0,
        );
        $fields = "c.*, c.id as typeid, count(s.id) as has_children, '' as children";
        $res = Db::name('arctype')
            ->field($fields)
            ->alias('c')
            ->join('arctype s','s.parent_id = c.id','LEFT')
            ->where($map)
            ->group('c.id')
            ->order('c.parent_id asc, c.sort_order asc, c.id')
            ->select()->toArray();
        if ($res) {
            $ctl_name_list = $ChannelType->getAll('id,ctl_name', array(), 'id');
            foreach ($res as $key => $val) {
                // 获取指定路由模式下的URL
                if ($val['is_part'] == 1) {
                    $val['typeurl'] = $val['typelink'];
                    if (!is_http_url($val['typeurl'])) {
                        $typeurl = '//'.request()->host();
                        if (!preg_match('#^(.*)$#i', $val['typeurl'])) {
                            $typeurl .= '';
                        }
                        $typeurl .= '/'.trim($val['typeurl'], '/');
                        $val['typeurl'] = $typeurl;
                    }
                } else {
                    $ctl_name = $ctl_name_list[$val['current_channel']]['ctl_name'];
                    $val['typeurl'] = typeurl($ctl_name."/lists", $val);
                }
                // 标记栏目被选中效果
                if ($val['id'] == $typeid || $val['id'] == $this->tid) {
                    $val['currentstyle'] = $this->currentstyle;
                } else {
                    $val['currentstyle'] = '';
                }
                // 封面图
                $val['litpic'] = handle_subdir_pic($val['litpic']);
                $res[$key] = $val;
            }
        }
        // 栏目层级归类成阶梯式
        $arr = group_same_key($res, 'parent_id');
        for ($i=0; $i < $arctype_max_level; $i++) {
            foreach ($arr as $key => $val) {
                foreach ($arr[$key] as $key2 => $val2) {
                    if (!isset($arr[$val2['id']])) continue;
                    $val2['children'] = $arr[$val2['id']];
                    $arr[$key][$key2] = $val2;
                }
            }
        }
        // 取得指定栏目ID对应的阶梯式所有子孙等栏目
        $result = array();
        $typeidArr = explode(',', $typeid);
        foreach ($typeidArr as $key => $val) {
            if (!isset($arr[$val])) continue;
            if (is_array($arr[$val])) {
                foreach ($arr[$val] as $key2 => $val2) {
                    array_push($result, $val2);
                }
            } else {
                array_push($result, $arr[$val]);
            }
        }
        // 没有子栏目时，获取同级栏目
        if (empty($result) && $self == true) {
            $result = $this->getSelf($typeid);
        }
        return $result;
    }

    // 获取当前栏目的第一级栏目下的子栏目
    public function getFirst($typeid)
    {
        $Arctype = new \app\common\model\Arctype;
        $result = array();
        if (empty($typeid)) {
            return $result;
        }
        $row = $Arctype->getAllPid($typeid); // 当前栏目往上一级级父栏目
        if (!empty($row)) {
            reset($row); // 重置排序
            $firstResult = current($row); // 顶级栏目下的第一级父栏目
            $typeid = isset($firstResult['id']) ? $firstResult['id'] : '';
            $sonRow = $this->getSon($typeid, false); // 获取第一级栏目下的子孙栏目，为空时不获得同级栏目
            $result = $sonRow;
        }
        return $result;
    }

    // 获取同级栏目
    public function getSelf($typeid)
    {
        $result = array();
        if (empty($typeid)) {
            return $result;
        }
        $map=[];
        // 获取指定栏目ID的上一级栏目ID列表
        $map[]=['id','in',$typeid];
        $map[]=['is_hidden','=',0];
        $map[]=['status','=',1];
        $map[]=['is_del','=',0];
        $res = Db::name('arctype')->field('parent_id')
            ->where($map)
            ->group('parent_id')
            ->select()->toArray();
        // 获取上一级栏目ID对应下的子孙栏目
        if ($res) {
            $typeidArr = get_arr_column($res, 'parent_id');
            $typeid = implode(',', $typeidArr);
            if ($typeid == 0) {
                $result = $this->getTop();
            } else {
                $result = $this->getSon($typeid, false);
            }
        }
        return $result;
    }

    // 获取顶级栏目
    public function getTop($notypeid = '')
    {
        $pcfglobal = get_global();
        $result = array();
        // 获取所有栏目
        $arctypeLogic = new \app\common\logic\ArctypeLogic();
        $ChannelType = new \app\common\model\ChannelType;
        $arctype_max_level = intval($pcfglobal['arctype_max_level']);
        $map = [];
        $map[]=['is_hidden','=',0];
        $map[]=['is_del','=',0];
        $map[]=['status','=',1];
        !empty($notypeid) && $map[] = ['id','<>', $notypeid]; // 排除指定栏目ID
        $res = $arctypeLogic->arctype_list(0, 0, false, $arctype_max_level, $map);
        if (count($res) > 0) {
            $topTypeid = $this->getTopTypeid($this->tid);
            $ctl_name_list = $ChannelType->getAll('id,ctl_name', array(), 'id');
            $currentstyleArr = ['tid' => 0,'currentstyle' => '','grade' => 100,'is_part' => 0,]; // 标记选择栏目的数组
            foreach ($res as $key => $val) {
                // 获取指定路由模式下的URL
                if ($val['is_part'] == 1) {
                    $val['typeurl'] = $val['typelink'];
                    if (!is_http_url($val['typeurl'])) {
                        $typeurl = '//'.request()->host();
                        if (!preg_match('#^(.*)$#i', $val['typeurl'])) {
                            $typeurl .= '';
                        }
                        $typeurl .= '/'.trim($val['typeurl'], '/');
                        $val['typeurl'] = $typeurl;
                    }
                } else {
                    $ctl_name = $ctl_name_list[$val['current_channel']]['ctl_name'];
                    $val['typeurl'] = typeurl($ctl_name."/lists", $val);
                }
                // 标记栏目被选中效果
                $val['currentstyle'] = '';
                $pageurl = request()->url(true);
                $typelink = htmlspecialchars_decode($val['typelink']);
                if ($val['id'] == $topTypeid || (!empty($typelink) && stristr($pageurl, $typelink))) {
                    $is_currentstyle = false;
                    if ($topTypeid != $this->tid && 0 == $currentstyleArr['is_part'] && $val['grade'] <= $currentstyleArr['grade']) { 
                    // 当前栏目不是顶级栏目，按外部链接优先
                        $is_currentstyle = true;
                    }
                    else if ($topTypeid == $this->tid && $val['grade'] < $currentstyleArr['grade']) { 
                    // 当前栏目是顶级栏目，按顺序优先
                        $is_currentstyle = true;
                    }
                    if ($is_currentstyle) {
                        $currentstyleArr = [
                            'tid'   => $val['id'],
                            'currentstyle'  => $this->currentstyle,
                            'grade' => $val['grade'],
                            'is_part'   => $val['is_part'],
                        ];
                    }
                }
                // 封面图
                $val['litpic'] = handle_subdir_pic($val['litpic']);
                $res[$key] = $val;
            }
            // 循环处理选中栏目的标识
            foreach ($res as $key => $val) {
                if (!empty($currentstyleArr) && $val['id'] == $currentstyleArr['tid']) {
                    $val['currentstyle'] = $currentstyleArr['currentstyle'];
                }
                $res[$key] = $val;
            }
            // 栏目层级归类成阶梯式
            $arr = group_same_key($res, 'parent_id');
            for ($i=0; $i < $arctype_max_level; $i++) { 
                foreach ($arr as $key => $val) {
                    foreach ($arr[$key] as $key2 => $val2) {
                        if (!isset($arr[$val2['id']])) continue;
                        $val2['children'] = $arr[$val2['id']];
                        $arr[$key][$key2] = $val2;
                    }
                }
            }
            reset($arr);  // 重置数组
            // 获取第一个数组
            $firstResult = current($arr);
            $result = $firstResult;
        }
        return $result;
    }

    // 获取最顶级父栏目ID
    public function getTopTypeid($typeid)
    {
        $Arctype = new \app\common\model\Arctype;
        $topTypeId = 0;
        if ($typeid > 0) {
            $result = $Arctype->getAllPid($typeid); // 当前栏目往上一级级父栏目
            reset($result); // 重置数组
            // 获取最顶级父栏目ID
            $firstVal = current($result);
            $topTypeId = $firstVal['id'];
        }
        return intval($topTypeId);
    }

}