<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;

// 在内容页模板追加显示浏览量
class TagArcclick extends Base
{
    public $aid = 0;

    //初始化
    protected function _initialize()
    {
        parent::_initialize();
        $this->aid = input('param.aid/d', 0);
    }

    /**
     * 在内容页模板追加显示浏览量
     */
    public function getArcclick($aid = '', $value = '', $type = '')
    {
        $domain = request()->domain()."/api.php".url('/Ajax/arcclick');
        $aid = !empty($aid) ? intval($aid) : $this->aid;
        if (empty($aid)) {
            return '标签arcclick报错：缺少属性 aid 值。';
        }
        //内容页或者其他页
        if (empty($type)) {
            if (!empty($this->aid)) {
                $type = 'view';
            } else {
                $type = 'lists';
            }
        }
        static $arcclick_js = null;



        if ('buildarticle' == strtolower(request()->action()) || null === $arcclick_js) {
            $arcclick_js = <<<EOF
<script type="text/javascript">
    function tag_arcclick(aid)
    {
        //步骤一:创建异步对象
        var ajax = new XMLHttpRequest();
        //步骤二:设置请求的url参数,参数一是请求的类型,参数二是请求的url,可以带参数,动态的传递参数starName到服务端
        ajax.open("get", "{$domain}?aid="+aid+"&type={$type}", true);
        // 给头部添加ajax信息
        ajax.setRequestHeader("X-Requested-With","XMLHttpRequest");
        //步骤三:发送请求
        ajax.send();
        //步骤四:注册事件 onreadystatechange 状态改变就会调用
        ajax.onreadystatechange = function () {
            //步骤五 如果能够进到这个判断 说明 数据 完美的回来了,并且请求的页面是存在的
            if (ajax.readyState==4 && ajax.status==200) {
        　　　　document.getElementById("gzpcf_arcclick_"+aid).innerHTML = ajax.responseText;
          　}
        } 
    }
</script>
EOF;
        } else {
            $arcclick_js = '';
        }
        $parseStr = <<<EOF
<i id="gzpcf_arcclick_{$aid}" class="gzpcf_arcclick" style="font-style:normal"></i> 
<script type="text/javascript">tag_arcclick({$aid});</script>
EOF;
        $parseStr = $arcclick_js . $parseStr;
        return $parseStr;
    }
}