<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;
use \think\Config;
use think\facade\Db;
use think\facade\Request;
use think\facade\Cache;

class TagGlobal extends Base
{
    //初始化
    protected function _initialize()
    {
        parent::_initialize();
    }

    // 获取全局变量
    public function getGlobal($name = '')
    {
        if (empty($name)) {
            return '标签global报错：缺少属性 name 。';
        }
        $param = explode('|', $name);
        $name = trim($param[0], '$');
        $value = '';
        $uiset = input('param.uiset/s', 'off');
        $uiset = trim($uiset, '/');

        // PC端与手机端的变量名自适应，可彼此通用
        if (in_array($name, ['web_thirdcode_pc','web_thirdcode_wap'])) { 
            // 第三方代码
            $name = 'web_thirdcode_' . (isMobile() ? 'wap' : 'pc');
        }

        static $globalTpCache = null;
        null === $globalTpCache && $globalTpCache = tpCache('global');
        $is_gzpcf_authortoken = !empty(tpCache('web.web_is_authortoken')) ? tpCache('web.web_is_authortoken') : 0;
        $value = isset($globalTpCache[$name]) ? $globalTpCache[$name] : '';
        $globalArr = ['value' => $value, 'data'  => $globalTpCache];

        if (!empty($globalArr['data'])) {
            $value = $globalArr['value'];
            $globalData = $globalArr['data'];
            switch ($name) {
                
                case 'web_title':
                case 'web_copyright':
                    if($is_gzpcf_authortoken < 0){
                        if (!empty($value)) {
                            $value = $value.'_by pcfcms';
                        }
                    }else{
                        $value = $value;
                    }
                    break;
                case 'web_recordnum':
                    if (!empty($value)) {
                        $value = '<a href="http://www.beian.miit.gov.cn/" rel="nofollow" target="_blank">'.$value.'</a>';
                    }
                    break;
                case 'web_templets_pc':
                case 'web_templets_m':
                    $value = $this->root_dir.$value;
                    break;
                case 'web_thirdcode_pc':
                case 'web_thirdcode_wap':
                    $value = '';
                    break;
                default:
                    $value = handle_subdir_pic($value, 'html');
                    $value = handle_subdir_pic($value);
                    break;
            }
            foreach ($param as $key => $val) {
                if ($key == 0) continue;
                $value = $val($value);
            }
            $value = htmlspecialchars_decode($value);
        }


        return $value;
    }
}