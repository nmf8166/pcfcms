<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;
use think\facade\Db;

class TagAdv extends Base
{
    //初始化
    protected function _initialize()
    {
        parent::_initialize();
    }

    // 获取广告
    public function getAdv($pid = '', $where = '', $orderby = '')
    {
        if (empty($pid)) {
            echo '标签adv报错：缺少属性 pid 。';
            return false;
        }
        $uiset = input('param.uiset/s', 'off');
        $uiset = trim($uiset, '/');
        $times = time();
        if (empty($where)) { // 新逻辑
            switch ($orderby) {
                case 'hot':
                case 'click':
                    $orderby = 'a.click desc';
                    break;
                case 'now':
                case 'new':
                    $orderby = 'a.add_time desc';
                    break;
                case 'id':
                    $orderby = 'a.id desc';
                    break;
                case 'sort_order':
                    $orderby = 'a.sort_order asc';
                    break;
                case 'rand':
                    $orderby = 'rand()';
                    break;
                default:
                    if (empty($orderby)) {
                        $orderby = 'a.sort_order asc, a.id desc';
                    }
                    break;
            }
            $where = "b.status = 1 AND a.pid={$pid} and a.start_time < {$times} and (a.end_time > {$times} OR a.end_time = 0) and a.status = 1";
            $result = Db::name("ad")->alias('a')
                ->field("a.*")
                ->join('ad_position b', 'b.id = a.pid', 'LEFT')
                ->where($where)
                ->orderRaw($orderby)
                ->select()->toArray();
        } else {
            switch ($orderby) {
                case 'hot':
                case 'click':
                    $orderby = 'click desc';
                    break;
                case 'now':
                case 'new':
                    $orderby = 'add_time desc';
                    break;
                case 'id':
                    $orderby = 'id desc';
                    break;
                case 'sort_order':
                    $orderby = 'sort_order asc';
                    break;
                case 'rand':
                    $orderby = 'rand()';
                    break;
                default:
                    if (empty($orderby)) {
                        $orderby = 'sort_order asc, id desc';
                    }
                    break;
            }
            $result = Db::name("ad")->field("*")
                ->where($where)
                ->orderRaw($orderby)
                ->select();
            $adpRow = Db::name("ad_position")->where(['id'=>$pid, 'status'=>1])->count();
            if (empty($adpRow)) {
                return false;
            }
        }
        foreach ($result as $key => $val) {
            $val['litpic'] = handle_subdir_pic(get_default_pic($val['litpic'])); // 默认无图封面
            $val['target'] = ($val['target'] == 1) ? 'target="_blank"' : 'target="_self"';
            $val['intro'] = htmlspecialchars_decode($val['intro']);
            $val['intro'] = handle_subdir_pic($val['intro'], 'html');
            if ($uiset == 'on') {
                $val['links'] = "javascript:void(0);";
            }
            $result[$key] = $val;
        }
        return $result;
    }
}