<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;
use think\facade\Db;
use app\home\logic\FieldLogic;
use app\common\model\Arctype;

class TagType extends Base
{
    public $tid = '';
    public $fieldLogic;
    
    //初始化
    protected function _initialize()
    {
        parent::_initialize();
        $this->fieldLogic = new FieldLogic();
        $this->Arctype = new Arctype();
        $this->gzpcfglobal = get_global(); // 获取配置参数
        $this->tid = input("param.tid/s", ''); // 应用于栏目列表
        // 应用于文档列表
        $aid = input('param.aid/d', 0);
        if ($aid > 0) {
            $this->tid = Db::name('archives')->where('aid', $aid)->value('typeid');
        }
        // typeid|tid为目录名称的情况下
        $this->tid = $this->getTrueTypeid($this->tid);
    }

    // 获取栏目基本信息
    public function getType($typeid = '', $type = 'self', $addfields = '')
    {
        $typeid = !empty($typeid) ? $typeid : $this->tid;
        if (empty($typeid)) {
            echo '标签type报错：缺少属性 typeid 值，或栏目ID不存在。';
            return false;
        }
        $result = array();
        switch ($type) {
            case 'top':
                $result = $this->getTop($typeid);
                break;
            default:
                $result = $this->getSelf($typeid);
                break;
        }
        $result = $this->fieldLogic->getTableFieldList($result, $this->gzpcfglobal['arctype_channel_id']);
        // 当前单页栏目的内容信息
        if (!empty($addfields) && isset($result['nid']) && $result['nid'] == 'single') {
            $addfields = str_replace('single_content', 'content', $addfields); // 兼容1.0.9之前的版本
            $addfields = str_replace('，', ',', $addfields); // 替换中文逗号
            $addfields = trim($addfields, ',');
            $row = Db::name('single_content')->field($addfields)->where('typeid',$result['id'])->find();
            $row = $this->fieldLogic->getChannelFieldList($row, $result['current_channel']);
            $result = array_merge($row, $result);
        }
        return $result;
    }

    // 获取当前栏目基本信息
    public function getSelf($typeid)
    {
        $result = $this->Arctype->getInfo($typeid); // 当前栏目信息
        return $result;
    }

    // 获取当前栏目的第一级栏目基本信息
    public function getTop($typeid)
    {
        $parent_list = $this->Arctype->getAllPid($typeid); // 获取当前栏目的所有父级栏目
        $result = current($parent_list); // 第一级栏目
        return $result;
    }

}