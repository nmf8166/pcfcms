<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;

// 搜索表单
class TagSearchurl extends Base
{
    //初始化
    protected function _initialize()
    {
        parent::_initialize();
    }

    // 获取搜索表单
    public function getSearchurl()
    {
        $vars = array();
        $url = url("/Search/lists");
        // 静态打开
        if (2 == tpCache('seo.seo_pseudo')) {
            $result = '';
            // 手机端域名
            $goto = input('param.goto/s');
            $goto = trim($goto, '/');
            !empty($goto) && $result .= '" /><input type="hidden" name="goto" value="'.$goto;
        } else {
            $result = $url;
        }
        return $result;
    }
    
}