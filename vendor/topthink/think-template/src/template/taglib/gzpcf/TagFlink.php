<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;
use think\facade\Db;

class TagFlink extends Base
{
    //初始化
    protected function _initialize()
    {
        parent::_initialize();
    }

    // 获取友情链接
    public function getFlink($type = 'text', $limit = '')
    {
        if ($type == 'text') {
            $typeid = 1;
        } elseif ($type == 'image') {
            $typeid = 2;
        }
        $map = [];
        if (!empty($typeid)) {
            $map[] = ['typeid','=',$typeid];
        }
        if(!empty($limit)){
           $limit = $limit; 
        }else{
            $limit = 100;
        }
        $result = Db::name("links")->where($map)
            ->order('sort_order asc')
            ->limit($limit)
            ->select()->toArray();
        foreach ($result as $key => $val) {
            $val['logo'] = get_default_pic($val['logo']);
            $val['target'] = ($val['target'] == 1) ? 'target="_blank"' : '';
            $result[$key] = $val;
        }
        return $result;
    }
}