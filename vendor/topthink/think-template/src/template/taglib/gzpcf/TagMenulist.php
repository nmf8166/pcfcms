<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;
use think\facade\Db;
use think\facade\Request;
use think\facade\Cache;
use app\admin\logic\NavLogic;

// 菜单列表
class TagMenulist extends Base
{
    public $tid = '';
    public $navid = ''; // 当前URL对应的导航菜单ID
    public $topid = ''; // 当前URL对应的顶级导航菜单ID
    public $currentstyle = '';
    public $position_id = '';

    //初始化
    protected function _initialize()
    {
        parent::_initialize();
        $this->navigationlogic = new NavLogic();
        $this->tid = input("param.tid/s", '');
        $aid = input('param.aid/d', 0);
        if ($aid > 0) {
            $cacheKey = 'tagNav_'.strtolower('home_'.Request::controller().'_'.Request::action());
            $cacheKey .= "_{$aid}";
            $this->tid = Cache::get($cacheKey);
            if ($this->tid == false) {
                $this->tid = Db::name('archives')->where('aid', $aid)->column('typeid');
                Cache::set($cacheKey,$this->tid,PCFCMS_CACHE_TIME);
            }
        }
        $this->tid = $this->getTrueTypeid($this->tid);
    }

    /**
     * 获取指定级别的菜单列表
     * @param string type son表示下一级菜单,self表示同级菜单,top顶级菜单
     * @param boolean $self 包括自己本身
     */
    public function getmenulist($position_id = '', $navid = '', $type = '', $currentstyle = '', $notnavid = '')
    {
        $this->position_id = intval($position_id);
        // 获取当前URL的导航菜单ID、顶级菜单ID
        $navidData = $this->getTopidOrNavid($this->tid);
        $this->navid = !empty($navidData['navid']) ? $navidData['navid'] : 0;
        $this->topid = !empty($navidData['topid']) ? $navidData['topid'] : 0;
        $type = !empty($type) ? $type : 'top';
        $this->currentstyle = $currentstyle;
        $navid  = !empty($navid) ? $navid : $this->navid;
        $result = $this->getSwitchType($navid, $type, $notnavid);
        return $result;
    }

    /**
     * 获取指定级别的菜单列表
     * @param string type son表示下一级菜单,self表示同级菜单,top顶级菜单
     * @param boolean $self 包括自己本身
     */
    public function getSwitchType($navid = '', $type = 'top', $notnavid = '')
    {
        $result = array();
        switch ($type) {
            case 'son': // 下级菜单
                $result = $this->getSon($navid, false);
                break;
            case 'self': // 同级菜单
                $result = $this->getSelf($navid);
                break;
            case 'top': // 顶级菜单
                $result = $this->getTop($notnavid);
                break;
            case 'sonself': // 下级、同级菜单
                $result = $this->getSon($navid, true);
                break;
            case 'first': // 第一级菜单
                $result = $this->getFirst($navid);
                break;
        }
        return $result;
    }

    // 获取下一级菜单 $self true表示没有子菜单时，获取同级菜单
    public function getSon($navid, $self = false)
    {
        $result = array();
        if (empty($navid)) {
            return $result;
        }
        $pcfglobal = get_global();
        $nav_max_level = $pcfglobal['nav_max_level'];
        // 获取所有显示且有效的菜单列表
        $map = [];
        $map[] = ['c.position_id','=',$this->position_id];
        $map[] = ['c.status','=',1];
        $map[] = ['c.is_del','=',0];
        $fields = "c.*, count(s.nav_id) as has_children, '' as children";
        $res = Db::name('nav_list')
            ->field($fields)
            ->alias('c')
            ->join('nav_list s','s.parent_id = c.nav_id','LEFT')
            ->where($map)
            ->group('c.nav_id')
            ->order('c.parent_id asc, c.sort_order asc, c.nav_id')
            ->select()->toArray();
        $res = convert_arr_key($res,'nav_id');
        if ($res) {
            // 所有栏目信息
            $arctypeRow = Db::name('arctype')->field('id,typename,dirname,current_channel,is_part,typelink')->select()->toArray();
            $arctypeRow = convert_arr_key($arctypeRow,'id');
            $ctl_name_list = Db::name('channel_type')->field('id,ctl_name')->select()->toArray();
            $ctl_name_list = convert_arr_key($ctl_name_list,'id');
            foreach ($res as $key => $val) {
                $arctypeInfo = !empty($arctypeRow[$val['type_id']]) ? $arctypeRow[$val['type_id']] : [];
                // 获取菜单的URL
                if (!empty($val['type_id']) && $val['type_id'] > 0) {
                    if (!empty($val['arctype_sync'])) { 
                        if ($arctypeInfo['is_part'] == 1) { 
                            $val['nav_url'] = $arctypeInfo['typelink'];//外部链接
                        } else {
                            if (!empty($arctypeInfo['type_id'])) {
                                $arctypeInfo2 = !empty($arctypeRow[$arctypeInfo['type_id']]) ? $arctypeRow[$arctypeInfo['type_id']] : [];
                                $ctl_name = $ctl_name_list[$arctypeInfo2['current_channel']]['ctl_name'];
                                $val['nav_url'] = typeurl($ctl_name."/lists", $arctypeInfo2);
                            } else {
                                $ctl_name = $ctl_name_list[$arctypeInfo['current_channel']]['ctl_name'];
                                $val['nav_url'] = typeurl($ctl_name."/lists", $arctypeInfo);
                            }
                            $val['nav_name'] = $arctypeInfo['typename'];
                            
                        }
                    }
                } else {
                    $val['nav_url'] = $arctypeInfo['typelink'] = $this->GetNavUrl($val['nav_url']);
                }
                // 标记菜单被选中效果
                if ($val['nav_id'] == $this->navid) {
                    $val['currentstyle'] = $this->currentstyle;
                } else {
                    $val['currentstyle'] = '';
                }
                $val['target'] = 1 == $val['target'] ? ' target="_blank" ' : '';
                $val['nofollow'] = 1 == $val['nofollow'] ? ' rel="nofollow" ' : '';
                // 封面图
                $val['nav_pic'] = handle_subdir_pic($val['nav_pic']);
                $res[$key] = $val;
            }
        }
        // 菜单层级归类成阶梯式
        $arr = group_same_key($res, 'parent_id');
        for ($i=0; $i < $nav_max_level; $i++) {
            foreach ($arr as $key => $val) {
                foreach ($arr[$key] as $key2 => $val2) {
                    if (!isset($arr[$val2['nav_id']])) continue;
                    $val2['children'] = $arr[$val2['nav_id']];
                    $arr[$key][$key2] = $val2;
                }
            }
        }
        // 取得指定菜单ID对应的阶梯式所有子孙等菜单
        $result = array();
        $navidArr = explode(',', $navid);
        foreach ($navidArr as $key => $val) {
            if (!isset($arr[$val])) continue;
            if (is_array($arr[$val])) {
                foreach ($arr[$val] as $key2 => $val2) {
                    array_push($result, $val2);
                }
            } else {
                array_push($result, $arr[$val]);
            }
        }
        // 没有子菜单时，获取同级菜单
        if (empty($result) && $self == true) {
            $result = $this->getSelf($navid);
        }
        return $result;
    }

    // 获取顶级菜单
    public function getTop($notnavid = '')
    {

        $result = array();
        // 获取所有菜单
        $pcfglobal = get_global();
        $nav_max_level = $pcfglobal['nav_max_level'];
        $map = [];
        $map[] = ['position_id','=',$this->position_id];
        $map[] = ['status','=',1];
        if(!empty($notnavid)){
            $map[] = ['nav_id','<>', $notnavid];
        }


        $res = $this->navigationlogic->nav_list(0, 0, false, $nav_max_level, $map);

        if (count($res) > 0) {
            // 所有栏目信息
            $arctypeRow = Db::name('arctype')->field('id,typename,dirname,current_channel,is_part,typelink')->select()->toArray();
            $arctypeRow = convert_arr_key($arctypeRow,'id');
            $ctl_name_list = Db::name('channel_type')->field('id,ctl_name')->select()->toArray();
            $ctl_name_list = convert_arr_key($ctl_name_list,'id');
            $currentstyleArr = [];
            foreach ($res as $key => $val) {
                $arctypeInfo = !empty($arctypeRow[$val['type_id']]) ? $arctypeRow[$val['type_id']] : [];
                // 获取菜单的URL
                if (!empty($val['type_id']) && $val['type_id'] > 0) {
                    if (!empty($val['arctype_sync'])) { 
                        if ($arctypeInfo['is_part'] == 1) { 
                            $val['nav_url'] = $arctypeInfo['typelink'];//外部链接
                        } else {
                                if (!empty($arctypeInfo['type_id'])) {
                                    $arctypeInfo2 = !empty($arctypeRow[$arctypeInfo['type_id']]) ? $arctypeRow[$arctypeInfo['type_id']] : [];
                                    $ctl_name = $ctl_name_list[$arctypeInfo2['current_channel']]['ctl_name'];
                                    $val['nav_url'] = typeurl($ctl_name."/lists", $arctypeInfo2);
                                } else {
                                    $ctl_name = $ctl_name_list[$arctypeInfo['current_channel']]['ctl_name'];
                                    $val['nav_url'] = typeurl($ctl_name."/lists", $arctypeInfo);
                                }
                                $val['nav_name'] = $arctypeInfo['typename'];
                            
                        }
                    }
                } else {
                    $val['nav_url'] = $arctypeInfo['typelink'] = $this->GetNavUrl($val['nav_url']);
                }
                // 标记菜单被选中效果
                $val['currentstyle'] = '';
                $nav_url = htmlspecialchars_decode($arctypeInfo['typelink']);
                if ($val['nav_id'] == $this->topid || $val['nav_id'] == $this->navid) {
                    $currentstyleArr[$val['nav_id']] = [
                        'nav_id'   => $val['nav_id'],
                        'currentstyle'  => $this->currentstyle,
                    ];
                }
                $val['target'] = 1 == $val['target'] ? ' target="_blank" ' : '';
                $val['nofollow'] = 1 == $val['nofollow'] ? ' rel="nofollow" ' : '';
                // 导航图片
                $val['navig_pic'] = handle_subdir_pic($val['nav_pic']);
                $res[$key] = $val;
            }
            // 循环处理选中菜单的标识
            foreach ($res as $key => $val) {
                $currentstyleInfo = !empty($currentstyleArr[$val['nav_id']]) ? $currentstyleArr[$val['nav_id']] : [];
                if (!empty($currentstyleInfo) && $val['nav_id'] == $currentstyleInfo['nav_id']) {
                    $val['currentstyle'] = $currentstyleInfo['currentstyle'];
                }
                $res[$key] = $val;
            }
            // 菜单层级归类成阶梯式
            $arr = group_same_key($res, 'parent_id');
            for ($i=0; $i < $nav_max_level; $i++) { 
                foreach ($arr as $key => $val) {
                    foreach ($arr[$key] as $key2 => $val2) {
                        if (!isset($arr[$val2['nav_id']])) continue;
                        $val2['children'] = $arr[$val2['nav_id']];
                        $arr[$key][$key2] = $val2;
                    }
                }
            }
            reset($arr);// 重置数组
            // 获取第一个数组
            $firstResult = current($arr);
            $result = $firstResult;
        }
        return $result;
    }

    // 获取当前菜单的第一级菜单下的子菜单
    public function getFirst($navid)
    {
        $result = array();
        if (empty($navid)) {
            return $result;
        }
        $row = Db::name('nav_list')->field('nav_id,topid,parent_id')->where('nav_id',$navid)->find(); 
        if (!empty($row)) {
            $navid = !empty($row['parent_id']) ? $row['topid'] : $row['nav_id'];
            $sonRow = $this->getSon($navid, false);
            $result = $sonRow;
        }
        return $result;
    }

    // 获取同级菜单
    public function getSelf($navid)
    {
        $result = array();
        if (empty($navid)) {
            return $result;
        }
        $map = [];
        $map[] = ['id','IN',$navid];
        $map[] = ['status','=',1];
        $map[] = ['is_del','=',0];
        $res = Db::name('nav_list')->field('parent_id')->where($map)->group('parent_id')->select()->toArray();
        // 获取上一级菜单ID对应下的子孙菜单
        if ($res) {
            $navidArr = convert_arr_key($res, 'parent_id');
            $navid = implode(',', $navidArr);
            if ($navid == 0) {
                $result = $this->getTop();
            } else {
                $result = $this->getSon($navid, false);
            }
        }
        return $result;
    }

    // 获取最顶级父菜单ID
    public function getTopidOrNavid($typeid = 0)
    {
        if ($typeid > 0) {
            $map1= [];
            $map1[]= ['type_id','=',$typeid];
            $map1[]= ['position_id','=',$this->position_id];
            $row = Db::name('nav_list')->field('nav_id,topid,parent_id')->where($map1)->find();
        } else {
            $code = 'home_'.Request::controller().'_'.Request::action();
            $map2= [];
            $map2[]= ['nav_url','=',$code];
            $map2[]= ['position_id','=',$this->position_id];
            $row = Db::name('nav_list')->field('nav_id,topid,parent_id')->where($map2)->find();
        }
        if (empty($row['parent_id'])) {
            $topid = $row['nav_id'];
        } else {
            $topid = $row['topid'];
        }
        $navid = $row['nav_id'];
        $data = ['topid' => $topid,'navid' => $navid];
        return $data;
    }

    // 获取URL
    public function GetNavUrl($nav_url)
    {
        $ReturnData = [];
        $data = $this->navigationlogic->ForegroundFunction();
        foreach ($data as $key => $val) {
            $ReturnData[$val['code']] = $val['url'];
        }
        return $ReturnData[$nav_url];
    }

}