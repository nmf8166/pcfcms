/*
MySQL Backup
Source Server Version: 5.7.27
Source Database: pcfcms
Date: 2020/9/4 星期五 16:06:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
--  Table structure for `pcf_ad`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_ad`;
CREATE TABLE `pcf_ad` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '广告id',
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '广告位置ID',
  `media_type` tinyint(1) DEFAULT '0' COMMENT '广告类型',
  `title` varchar(60) DEFAULT '' COMMENT '广告名称',
  `links` varchar(255) DEFAULT '' COMMENT '广告链接',
  `litpic` varchar(255) DEFAULT '' COMMENT '图片地址',
  `start_time` int(11) DEFAULT NULL COMMENT '投放时间',
  `end_time` int(11) DEFAULT NULL COMMENT '结束时间',
  `intro` text COMMENT '描述',
  `link_man` varchar(60) DEFAULT '' COMMENT '添加人',
  `link_email` varchar(60) DEFAULT '' COMMENT '添加人邮箱',
  `link_phone` varchar(60) DEFAULT '' COMMENT '添加人联系电话',
  `click` int(11) DEFAULT '0' COMMENT '点击量',
  `bgcolor` varchar(30) DEFAULT '' COMMENT '背景颜色',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '1=显示，0=屏蔽',
  `sort_order` int(11) DEFAULT '0' COMMENT '排序',
  `target` varchar(50) DEFAULT '' COMMENT '是否开启浏览器新窗口',
  `admin_id` int(10) DEFAULT '0' COMMENT '管理员ID',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '伪删除，1=是，0=否',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(255) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`),
  KEY `position_id` (`pid`) USING BTREE,
  KEY `status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='广告表';

-- ----------------------------
--  Table structure for `pcf_admin`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_admin`;
CREATE TABLE `pcf_admin` (
  `admin_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_name` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `pen_name` varchar(50) DEFAULT '' COMMENT '笔名（发布文章后显示责任编辑的名字）',
  `sex` varchar(10) DEFAULT NULL,
  `mobile` varchar(11) DEFAULT '' COMMENT '手机号码',
  `email` varchar(60) DEFAULT '' COMMENT 'email',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `head_pic` varchar(255) DEFAULT '' COMMENT '头像',
  `last_login` int(11) DEFAULT '0' COMMENT '最后登录时间',
  `last_ip` varchar(15) DEFAULT '' COMMENT '最后登录ip',
  `login_cnt` int(11) DEFAULT '0' COMMENT '登录次数',
  `role_id` int(10) NOT NULL DEFAULT '-1' COMMENT '角色组ID（-1表示超级管理员）',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态(0=屏蔽，1=正常)',
  `add_time` int(11) DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `parent_id` int(10) DEFAULT '0' COMMENT '父id',
  `typeid` int(11) unsigned DEFAULT '0' COMMENT '默认栏目id',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`admin_id`),
  KEY `user_name` (`user_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
--  Table structure for `pcf_admin_log`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_admin_log`;
CREATE TABLE `pcf_admin_log` (
  `log_id` bigint(16) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `admin_id` int(10) NOT NULL DEFAULT '-1' COMMENT '管理员id',
  `log_info` text COMMENT '日志描述',
  `log_ip` varchar(30) DEFAULT '' COMMENT 'ip地址',
  `log_osName` varchar(255) DEFAULT NULL COMMENT '设备类型',
  `log_device` varchar(255) DEFAULT '' COMMENT '设备',
  `log_time` int(11) DEFAULT '0' COMMENT '日志时间',
  `log_browserType` varchar(255) DEFAULT NULL COMMENT '浏览器',
  PRIMARY KEY (`log_id`),
  KEY `admin_id` (`admin_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='管理员操作日志表';

-- ----------------------------
--  Table structure for `pcf_ad_position`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_ad_position`;
CREATE TABLE `pcf_ad_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) NOT NULL DEFAULT '' COMMENT '广告位置名称',
  `content` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0关闭1开启',
  `admin_id` int(10) NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '伪删除，1=是，0=否',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='广告位置表';

-- ----------------------------
--  Table structure for `pcf_archives`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_archives`;
CREATE TABLE `pcf_archives` (
  `aid` int(10) NOT NULL AUTO_INCREMENT,
  `joinaid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '关联文档id',
  `typeid` int(10) NOT NULL DEFAULT '0' COMMENT '当前栏目',
  `channel` int(10) NOT NULL DEFAULT '0' COMMENT '模型ID',
  `title` varchar(200) NOT NULL DEFAULT '' COMMENT '标题',
  `litpic` varchar(250) NOT NULL DEFAULT '' COMMENT '缩略图',
  `is_b` tinyint(1) NOT NULL DEFAULT '0' COMMENT '加粗',
  `is_head` tinyint(1) NOT NULL DEFAULT '0' COMMENT '头条（0=否，1=是）',
  `is_special` tinyint(1) NOT NULL DEFAULT '0' COMMENT '特荐（0=否，1=是）',
  `is_top` tinyint(1) NOT NULL DEFAULT '0' COMMENT '置顶（0=否，1=是）',
  `is_recom` tinyint(1) NOT NULL DEFAULT '0' COMMENT '推荐（0=否，1=是）',
  `is_jump` tinyint(1) NOT NULL DEFAULT '0' COMMENT '跳转链接（0=否，1=是）',
  `is_litpic` tinyint(1) NOT NULL DEFAULT '0' COMMENT '图片（0=否，1=是）',
  `author` varchar(200) NOT NULL DEFAULT '' COMMENT '作者',
  `click` int(10) NOT NULL DEFAULT '0' COMMENT '浏览量',
  `arcrank` tinyint(1) NOT NULL DEFAULT '0' COMMENT '阅读权限：0=开放浏览，-1=待审核稿件',
  `jumplinks` varchar(200) NOT NULL DEFAULT '' COMMENT '外链跳转',
  `ismake` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否静态页面（0=动态，1=静态）',
  `seo_title` varchar(200) NOT NULL DEFAULT '' COMMENT 'SEO标题',
  `seo_keywords` varchar(200) NOT NULL DEFAULT '' COMMENT 'SEO关键词',
  `seo_description` text NOT NULL COMMENT 'SEO描述',
  `prom_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '产品类型：0普通产品，1虚拟产品',
  `tempview` varchar(200) NOT NULL DEFAULT '' COMMENT '文档模板文件名',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态(0=屏蔽，1=正常)',
  `sort_order` int(10) NOT NULL DEFAULT '0' COMMENT '排序号',
  `admin_id` int(10) NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '伪删除，1=是，0=否',
  `del_method` tinyint(1) NOT NULL DEFAULT '0' COMMENT '伪删除状态，1为主动删除，2为跟随上级栏目被动删除',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `users_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `show_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '置顶时间',
  `zzbaidu` int(2) DEFAULT '0',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`aid`),
  KEY `typeid` (`typeid`,`channel`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COMMENT='文档主表';

-- ----------------------------
--  Table structure for `pcf_arcmulti`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_arcmulti`;
CREATE TABLE `pcf_arcmulti` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `tagid` varchar(60) NOT NULL DEFAULT '' COMMENT '标签ID',
  `tagname` varchar(60) NOT NULL DEFAULT '' COMMENT '标签名',
  `innertext` text NOT NULL COMMENT '标签模板代码',
  `pagesize` int(10) NOT NULL DEFAULT '0' COMMENT '分页列表',
  `querysql` text NOT NULL COMMENT '完整SQL',
  `ordersql` varchar(200) DEFAULT '' COMMENT '排序SQL',
  `addfieldsSql` varchar(255) DEFAULT '' COMMENT '附加字段SQL',
  `addtableName` varchar(50) DEFAULT '' COMMENT '附加字段的数据表，不包含表前缀',
  `attstr` text COMMENT '属性字符串',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='多页标记存储数据表';

-- ----------------------------
--  Table structure for `pcf_arcrank`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_arcrank`;
CREATE TABLE `pcf_arcrank` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '权限ID',
  `rank` smallint(6) DEFAULT '0' COMMENT '权限值',
  `name` char(20) DEFAULT '' COMMENT '会员名称',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='文档阅读权限表';

-- ----------------------------
--  Table structure for `pcf_arctype`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_arctype`;
CREATE TABLE `pcf_arctype` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '栏目ID',
  `channeltype` int(10) DEFAULT '0' COMMENT '栏目顶级模型ID',
  `current_channel` int(10) DEFAULT '0' COMMENT '栏目当前模型ID',
  `parent_id` int(10) DEFAULT '0' COMMENT '栏目上级ID',
  `typename` varchar(200) DEFAULT '' COMMENT '栏目名称',
  `dirname` varchar(200) DEFAULT '' COMMENT '目录英文名',
  `dirpath` varchar(200) DEFAULT '' COMMENT '目录存放HTML路径',
  `englist_name` varchar(200) DEFAULT '' COMMENT '栏目英文名',
  `grade` tinyint(1) DEFAULT '0' COMMENT '栏目等级',
  `typelink` varchar(200) DEFAULT '' COMMENT '栏目链接',
  `litpic` varchar(250) DEFAULT '' COMMENT '栏目图片',
  `templist` varchar(200) DEFAULT '' COMMENT '列表模板文件名',
  `tempview` varchar(200) DEFAULT '' COMMENT '文档模板文件名',
  `seo_title` varchar(200) DEFAULT '' COMMENT 'SEO标题',
  `seo_keywords` varchar(200) DEFAULT '' COMMENT 'seo关键字',
  `seo_description` text COMMENT 'seo描述',
  `sort_order` int(10) DEFAULT '0' COMMENT '排序号',
  `is_hidden` tinyint(1) DEFAULT '0' COMMENT '是否隐藏栏目：0=显示，1=隐藏',
  `is_part` tinyint(1) DEFAULT '0' COMMENT '栏目属性：0=内容栏目，1=外部链接',
  `admin_id` int(10) DEFAULT '0' COMMENT '管理员ID',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '伪删除，1=是，0=否',
  `del_method` tinyint(1) DEFAULT '0' COMMENT '伪删除状态，1为主动删除，2为跟随上级栏目被动删除',
  `status` tinyint(1) DEFAULT '1' COMMENT '启用 (1=正常，0=屏蔽)',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dirname` (`dirname`) USING BTREE,
  KEY `parent_id` (`channeltype`,`parent_id`) USING BTREE,
  KEY `parent_id_2` (`parent_id`,`sort_order`,`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='文档栏目表';

-- ----------------------------
--  Table structure for `pcf_article_content`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_article_content`;
CREATE TABLE `pcf_article_content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aid` int(10) DEFAULT '0' COMMENT '文档ID',
  `content` longtext NOT NULL COMMENT '内容详情',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='文章附加表';

-- ----------------------------
--  Table structure for `pcf_auth_role`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_auth_role`;
CREATE TABLE `pcf_auth_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '' COMMENT '角色名',
  `remark` text COMMENT '备注信息',
  `permission` text COMMENT '已允许的权限',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态(1=正常，0=屏蔽)',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `online_update` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '在线升级',
  `only_oneself` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '只查看自己发布',
  `built_in` tinyint(1) DEFAULT '0' COMMENT '内置用户组，1表示内置',
  `admin_id` int(10) DEFAULT '0' COMMENT '操作员',
  `sort_order` int(10) DEFAULT '0' COMMENT '排序号',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`),
  KEY `admin_id` (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='管理员角色表';

-- ----------------------------
--  Table structure for `pcf_channelfield`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_channelfield`;
CREATE TABLE `pcf_channelfield` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '字段名称',
  `channel_id` int(10) NOT NULL DEFAULT '0' COMMENT '所属文档模型id',
  `short_name` varchar(10) NOT NULL DEFAULT '' COMMENT '简称',
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT '字段标题',
  `dtype` varchar(32) NOT NULL DEFAULT '' COMMENT '字段类型',
  `define` text NOT NULL COMMENT '字段定义',
  `maxlength` int(10) NOT NULL DEFAULT '0' COMMENT '最大长度，文本数据必须填写，大于255为text类型',
  `dfvalue` text NOT NULL COMMENT '默认值',
  `dfvalue_unit` varchar(50) NOT NULL DEFAULT '' COMMENT '数值单位',
  `remark` varchar(256) NOT NULL DEFAULT '' COMMENT '提示说明',
  `is_screening` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否应用于条件筛选',
  `is_order` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否应用于排序',
  `ifeditable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否在编辑页显示',
  `ifrequire` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否必填',
  `ifsystem` tinyint(1) NOT NULL DEFAULT '0' COMMENT '字段分类，1=系统(不可修改)，0=自定义',
  `ifmain` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否主表字段',
  `ifcontrol` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态，控制该条数据是否允许被控制，1为不允许控制，0为允许控制',
  `sort_order` int(5) NOT NULL DEFAULT '100' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `channel_id` (`channel_id`,`is_screening`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=656 DEFAULT CHARSET=utf8 COMMENT='自定义字段表';

-- ----------------------------
--  Table structure for `pcf_channelfield_bind`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_channelfield_bind`;
CREATE TABLE `pcf_channelfield_bind` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `typeid` int(10) DEFAULT '0' COMMENT '栏目ID',
  `field_id` int(10) DEFAULT '0' COMMENT '自定义字段ID',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型，1：筛选，2：排序',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=213 DEFAULT CHARSET=utf8 COMMENT='栏目与自定义字段绑定表';

-- ----------------------------
--  Table structure for `pcf_channel_type`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_channel_type`;
CREATE TABLE `pcf_channel_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nid` varchar(50) NOT NULL DEFAULT '' COMMENT '识别id',
  `title` varchar(30) DEFAULT '' COMMENT '名称',
  `ntitle` varchar(30) DEFAULT '' COMMENT '左侧菜单名称',
  `table` varchar(50) DEFAULT '' COMMENT '表名',
  `ctl_name` varchar(50) DEFAULT '' COMMENT '控制器名称（区分大小写）',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态(1=启用，0=屏蔽)',
  `ifsystem` tinyint(1) DEFAULT '0' COMMENT '字段分类，1=系统(不可修改)，0=自定义',
  `is_repeat_title` tinyint(1) DEFAULT '1' COMMENT '文档标题重复，1=允许，0=不允许',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '伪删除，1=是，0=否',
  `sort_order` smallint(6) DEFAULT '100' COMMENT '排序',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idention` (`nid`) USING BTREE,
  UNIQUE KEY `ctl_name` (`ctl_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='系统模型表';

-- ----------------------------
--  Table structure for `pcf_common_pic`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_common_pic`;
CREATE TABLE `pcf_common_pic` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '常用图片ID',
  `pid` char(32) DEFAULT NULL,
  `pic_path` varchar(255) NOT NULL DEFAULT '' COMMENT '图片地址',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COMMENT='常用图片';

-- ----------------------------
--  Table structure for `pcf_config`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_config`;
CREATE TABLE `pcf_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT '' COMMENT '配置的key键名',
  `value` text,
  `inc_type` varchar(64) DEFAULT '' COMMENT '配置分组',
  `desc` varchar(50) DEFAULT '' COMMENT '描述',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否已删除，0=否，1=是',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`),
  KEY `inc_type` (`inc_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=328 DEFAULT CHARSET=utf8 COMMENT='系统配置表';

-- ----------------------------
--  Table structure for `pcf_config_attribute`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_config_attribute`;
CREATE TABLE `pcf_config_attribute` (
  `attr_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '表单id',
  `inc_type` varchar(20) DEFAULT '' COMMENT '变量分组',
  `attr_name` varchar(60) DEFAULT '' COMMENT '变量标题',
  `attr_var_name` varchar(50) DEFAULT '' COMMENT '变量名',
  `attr_input_type` tinyint(1) unsigned DEFAULT '0' COMMENT ' 0=文本框，1=下拉框，2=多行文本框，3=上传图片',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`attr_id`),
  KEY `inc_type` (`inc_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='自定义变量表';

-- ----------------------------
--  Table structure for `pcf_field_type`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_field_type`;
CREATE TABLE `pcf_field_type` (
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '字段类型',
  `title` varchar(64) NOT NULL DEFAULT '' COMMENT '中文类型名',
  `ifoption` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否需要设置选项',
  `sort_order` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字段类型表';

-- ----------------------------
--  Table structure for `pcf_guestbook`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_guestbook`;
CREATE TABLE `pcf_guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `need_login` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='留言主表';

-- ----------------------------
--  Table structure for `pcf_guestbook_attr`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_guestbook_attr`;
CREATE TABLE `pcf_guestbook_attr` (
  `attr_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '表单id',
  `attr_name` varchar(60) DEFAULT '' COMMENT '字段名称',
  `form_id` int(11) unsigned DEFAULT '0' COMMENT '栏目ID',
  `input_type` varchar(32) DEFAULT '' COMMENT '字段类型',
  `attr_values` text COMMENT '可选值列表',
  `sort_order` int(11) unsigned DEFAULT '100' COMMENT '表单排序',
  `is_fill` tinyint(1) unsigned DEFAULT '0' COMMENT '是否必填，1：必填',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否已删除，0=否，1=是',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`attr_id`),
  KEY `form_id` (`form_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COMMENT='表单字段表';

-- ----------------------------
--  Table structure for `pcf_guestbook_list`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_guestbook_list`;
CREATE TABLE `pcf_guestbook_list` (
  `list_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '表单id',
  `md5data` varchar(50) NOT NULL,
  `ip` varchar(20) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '' COMMENT 'ip所在城市',
  `is_read` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已读',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '伪删除，1=是，0=否',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`list_id`),
  KEY `form_id` (`form_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='表单列表';

-- ----------------------------
--  Table structure for `pcf_guestbook_value`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_guestbook_value`;
CREATE TABLE `pcf_guestbook_value` (
  `value_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '留言表单id自增',
  `form_id` int(11) unsigned NOT NULL DEFAULT '0',
  `list_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '留言id',
  `attr_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '表单id',
  `attr_value` text COMMENT '表单值',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`value_id`),
  KEY `form_id` (`form_id`,`list_id`,`attr_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COMMENT='表单内容表';

-- ----------------------------
--  Table structure for `pcf_images`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_images`;
CREATE TABLE `pcf_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` char(32) DEFAULT NULL COMMENT '图片ID',
  `name` varchar(50) DEFAULT NULL COMMENT '图片名称',
  `url` varchar(255) DEFAULT NULL COMMENT '绝对地址',
  `path` varchar(255) DEFAULT NULL COMMENT '物理地址',
  `type` enum('web','local','Aliyun') DEFAULT 'local' COMMENT '存储引擎',
  `ctime` bigint(12) unsigned DEFAULT NULL COMMENT '创建时间',
  `isdel` bigint(12) unsigned DEFAULT NULL COMMENT '删除标志 有数据代表删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`pid`) USING BTREE,
  KEY `id_2` (`pid`) USING BTREE,
  KEY `isdel` (`isdel`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='图片表';

-- ----------------------------
--  Table structure for `pcf_links`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_links`;
CREATE TABLE `pcf_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typeid` tinyint(1) DEFAULT '1' COMMENT '类型：1=文字链接，2=图片链接',
  `title` varchar(50) DEFAULT '' COMMENT '网站标题',
  `url` varchar(100) DEFAULT '' COMMENT '网站地址',
  `logo` varchar(255) DEFAULT '' COMMENT '网站LOGO',
  `sort_order` int(11) DEFAULT '0' COMMENT '排序号',
  `target` tinyint(1) DEFAULT '0' COMMENT '是否开启浏览器新窗口',
  `email` varchar(50) DEFAULT NULL,
  `intro` text COMMENT '网站简况',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态(1=显示，0=屏蔽)',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='友情链接表';

-- ----------------------------
--  Table structure for `pcf_menu`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_menu`;
CREATE TABLE `pcf_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一性标识',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级ID',
  `name` varchar(30) CHARACTER SET utf8mb4 NOT NULL COMMENT '菜单名称',
  `icon` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '图标',
  `url` varchar(150) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'URL地址',
  `param` varchar(150) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '参数',
  `auth` varchar(150) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '权限标识',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '类型：1模块 2导航 3菜单 4节点',
  `sort` smallint(5) unsigned DEFAULT '125' COMMENT '显示顺序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int(10) unsigned DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  `is_show` tinyint(1) unsigned DEFAULT '1' COMMENT '是否显示：1显示 2不显示',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='后台菜单节点管理表';

-- ----------------------------
--  Table structure for `pcf_nav_list`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_nav_list`;
CREATE TABLE `pcf_nav_list` (
  `nav_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '导航ID',
  `topid` int(10) NOT NULL DEFAULT '0' COMMENT '顶级菜单ID',
  `parent_id` int(10) NOT NULL DEFAULT '0' COMMENT '父级ID',
  `nav_name` varchar(200) NOT NULL DEFAULT '' COMMENT '导航名称',
  `englist_name` varchar(200) NOT NULL DEFAULT '' COMMENT '英文名称',
  `nav_url` varchar(200) NOT NULL DEFAULT '' COMMENT '导航链接',
  `position_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '导航位置',
  `arctype_sync` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否与栏目同步',
  `type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '同步栏目的ID',
  `nav_pic` varchar(255) NOT NULL DEFAULT '' COMMENT '导航图片',
  `grade` tinyint(1) NOT NULL DEFAULT '0' COMMENT '菜单等级',
  `target` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否打开新窗口，1=是，0=否',
  `nofollow` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否使用nofollow，1=是，0=否',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '启用 (1=正常，0=停用)',
  `is_del` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '伪删除，1=是，0=否',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序号',
  `add_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`nav_id`),
  KEY `type_id` (`type_id`),
  KEY `position_id` (`position_id`,`status`,`is_del`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COMMENT='导航列表';

-- ----------------------------
--  Table structure for `pcf_nav_position`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_nav_position`;
CREATE TABLE `pcf_nav_position` (
  `position_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '导航列表ID',
  `position_name` varchar(200) NOT NULL DEFAULT '' COMMENT '导航列表名称',
  `sort_order` int(10) NOT NULL DEFAULT '0' COMMENT '排序号',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '伪删除，1=是，0=否',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='导航位置表';

-- ----------------------------
--  Table structure for `pcf_quickentry`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_quickentry`;
CREATE TABLE `pcf_quickentry` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT '' COMMENT '名称',
  `laytext` varchar(50) DEFAULT '' COMMENT '完整标题',
  `type` smallint(5) DEFAULT '0' COMMENT '归类，1=快捷入口，2=内容统计',
  `controller` varchar(20) DEFAULT '' COMMENT '控制器名',
  `action` varchar(20) DEFAULT '' COMMENT '操作名',
  `vars` varchar(100) DEFAULT '' COMMENT 'URL参数字符串',
  `groups` smallint(5) DEFAULT '0' COMMENT '分组，1=模型',
  `checked` tinyint(4) DEFAULT '0' COMMENT '选中，0=否，1=是',
  `issystem` tinyint(1) DEFAULT NULL COMMENT '状态，1=系统模型，0=频道模型',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态，1=有效，0=无效',
  `sort_order` int(10) DEFAULT '0' COMMENT '排序',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `type` (`type`,`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='快捷入口表';

-- ----------------------------
--  Table structure for `pcf_search_word`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_search_word`;
CREATE TABLE `pcf_search_word` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `word` varchar(50) DEFAULT '' COMMENT '关键词',
  `searchNum` int(10) DEFAULT '1' COMMENT '搜索次数',
  `sort_order` int(10) DEFAULT '0' COMMENT '排序号',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`),
  KEY `word` (`word`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='搜索词统计表';

-- ----------------------------
--  Table structure for `pcf_single_content`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_single_content`;
CREATE TABLE `pcf_single_content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文档ID',
  `typeid` int(10) DEFAULT '0' COMMENT '栏目ID',
  `content` longtext NOT NULL COMMENT '内容详情',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='单页附加表';

-- ----------------------------
--  Table structure for `pcf_sms_log`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_sms_log`;
CREATE TABLE `pcf_sms_log` (
  `record_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `source` tinyint(1) DEFAULT '0' COMMENT '来源，与场景ID对应：0=默认，2=注册，3=绑定手机，4=找回密码',
  `mobile` varchar(50) DEFAULT '' COMMENT '手机号码',
  `users_id` int(10) DEFAULT '0' COMMENT '用户ID',
  `code` varchar(20) DEFAULT '' COMMENT '发送手机内容',
  `status` tinyint(1) DEFAULT '0' COMMENT '是否使用，默认0，0为未使用，1为使用',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='手机发送记录表';

-- ----------------------------
--  Table structure for `pcf_smtp_log`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_smtp_log`;
CREATE TABLE `pcf_smtp_log` (
  `record_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `source` tinyint(1) DEFAULT '0' COMMENT '来源，与场景ID对应：0=默认，2=注册，3=绑定邮箱，4=找回密码',
  `email` varchar(50) DEFAULT '' COMMENT '邮件地址',
  `users_id` int(10) DEFAULT '0' COMMENT '用户ID',
  `code` varchar(20) DEFAULT '' COMMENT '发送邮件内容',
  `status` tinyint(1) DEFAULT '0' COMMENT '是否使用，默认0，0为未使用，1为使用',
  `add_time` int(11) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='邮件发送记录表';

-- ----------------------------
--  Table structure for `pcf_task`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_task`;
CREATE TABLE `pcf_task` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `title` varchar(255) NOT NULL COMMENT '任务标题',
  `content` varchar(255) NOT NULL COMMENT '执行内容',
  `start_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '开始执行时间',
  `stop_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  `maxnum` int(100) unsigned NOT NULL COMMENT '最多执行',
  `num` int(100) unsigned NOT NULL COMMENT '执行次数',
  `add_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='计划任务';

-- ----------------------------
--  Table structure for `pcf_update_config`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_update_config`;
CREATE TABLE `pcf_update_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '功能配置表ID',
  `name` varchar(50) DEFAULT '' COMMENT '配置的key键名',
  `value` text COMMENT '配置的value值',
  `desc` varchar(100) DEFAULT '' COMMENT '键名说明',
  `inc_type` varchar(64) DEFAULT '' COMMENT '配置分组',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='更新配置表';

-- ----------------------------
--  Table structure for `pcf_users`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_users`;
CREATE TABLE `pcf_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level_id` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '默认会员等级为注册会员',
  `open_id` varchar(30) NOT NULL DEFAULT '' COMMENT '微信唯一标识openid',
  `username` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `truename` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `is_mobile` tinyint(1) DEFAULT '0' COMMENT '绑定手机号，0为不绑定，1为绑定',
  `mobile` varchar(11) DEFAULT '' COMMENT '手机号码',
  `is_email` tinyint(1) DEFAULT '0' COMMENT '绑定邮箱，0为不绑定，1为绑定',
  `email` varchar(60) DEFAULT '' COMMENT 'email',
  `qq` varchar(13) NOT NULL DEFAULT '' COMMENT 'qq号码',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `litpic` varchar(255) DEFAULT '' COMMENT '头像',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态(0=屏蔽，1=正常)',
  `add_time` int(11) DEFAULT '0' COMMENT '添加时间',
  `login_time` int(11) DEFAULT '0' COMMENT '登陆时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `is_del` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `register_place` tinyint(1) DEFAULT '2' COMMENT '注册位置。后台注册不受注册验证影响，1为后台注册，2为前台注册。默认为2。',
  `admin_id` int(10) DEFAULT '0' COMMENT '关联管理员ID',
  `remark` varchar(255) DEFAULT NULL,
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='会员表';

-- ----------------------------
--  Table structure for `pcf_users_config`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_users_config`;
CREATE TABLE `pcf_users_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '会员功能配置表ID',
  `name` varchar(50) DEFAULT '' COMMENT '配置的key键名',
  `value` text COMMENT '配置的value值',
  `desc` varchar(100) DEFAULT '' COMMENT '键名说明',
  `inc_type` varchar(64) DEFAULT '' COMMENT '配置分组',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='会员配置表';

-- ----------------------------
--  Table structure for `pcf_users_level`
-- ----------------------------
DROP TABLE IF EXISTS `pcf_users_level`;
CREATE TABLE `pcf_users_level` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level_name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `free_day_send` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '每天免费发布',
  `free_all_send` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '共免费发布',
  `fee_day_top` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '每天免费置顶',
  `fee_all_top` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '共免费置顶',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态（1：启用，0：关闭）',
  `add_time` int(11) DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `is_del` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '注册默认级别',
  `lang` varchar(50) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='会员级别表';

-- ----------------------------
--  Records 
-- ----------------------------
INSERT INTO `pcf_arcrank` VALUES ('1','0','开放浏览','0','1552376880','cn'), ('2','-1','待审核稿件','0','1552376880','cn');
INSERT INTO `pcf_auth_role` VALUES ('13','内容管理员','','a:23:{i:0;s:1:\"6\";i:4;s:2:\"36\";i:6;s:2:\"38\";i:13;s:2:\"41\";i:17;s:2:\"44\";i:19;s:2:\"46\";i:21;s:2:\"50\";i:26;s:2:\"54\";i:31;s:3:\"148\";i:33;s:2:\"58\";i:37;s:2:\"71\";i:41;s:2:\"79\";i:44;s:2:\"67\";i:48;s:3:\"203\";i:49;s:3:\"159\";i:54;s:2:\"75\";i:59;s:2:\"81\";i:63;s:2:\"85\";i:68;s:2:\"89\";i:73;s:2:\"99\";i:75;s:3:\"101\";i:80;s:3:\"105\";i:85;s:3:\"113\";}','1','1586769600','1598863763','0','0','0','1','0','');
INSERT INTO `pcf_channelfield` VALUES ('1','add_time','0','','新增时间','datetime','int(11)','250','','','','0','0','1','0','1','1','1','100','1','1533091575','1533091575'), ('2','update_time','0','','更新时间','datetime','int(11)','250','','','','0','0','1','0','1','1','1','100','1','1533091601','1533091601'), ('3','aid','0','','文档ID','int','int(11)','250','','','','0','0','1','0','1','1','1','100','1','1533091624','1533091624'), ('4','typeid','0','','当前栏目ID','int','int(11)','250','','','','0','0','1','0','1','1','1','100','1','1533091930','1533091930'), ('5','channel','0','','模型ID','int','int(11)','250','','','','0','0','1','0','1','1','1','100','1','1533092214','1533092214'), ('6','is_b','0','','是否加粗','switch','tinyint(1)','250','','','','0','0','1','0','1','1','1','100','1','1533092246','1533092246'), ('7','title','0','','文档标题','text','varchar(250)','250','','','','0','0','1','0','1','1','1','100','1','1533092381','1533092381'), ('8','litpic','0','','缩略图','img','varchar(250)','250','','','','0','0','1','0','1','1','1','100','1','1533092398','1533092398'), ('9','is_head','0','','是否头条','switch','tinyint(1)','250','','','','0','0','1','0','1','1','1','100','1','1533092420','1533092420'), ('10','is_special','0','','是否特荐','switch','tinyint(1)','250','','','','0','0','1','0','1','1','1','100','1','1533092439','1533092439'), ('11','is_top','0','','是否置顶','switch','tinyint(1)','250','','','','0','0','1','0','1','1','1','100','1','1533092454','1533092454'), ('12','is_recom','0','','是否推荐','switch','tinyint(1)','250','','','','0','0','1','0','1','1','1','100','1','1533092468','1533092468'), ('13','is_jump','0','','是否跳转','switch','tinyint(1)','250','','','','0','0','1','0','1','1','1','100','1','1533092484','1533092484'), ('14','author','0','','编辑者','text','varchar(250)','250','','','','0','0','1','0','1','1','1','100','1','1533092498','1533092498'), ('15','click','0','','浏览量','int','int(11)','250','','','','0','0','1','0','1','1','1','100','1','1533092512','1533092512'), ('16','arcrank','0','','阅读权限','select','tinyint(1)','250','','','','0','0','1','0','1','1','1','100','1','1533092534','1533092534'), ('17','jumplinks','0','','跳转链接','text','varchar(250)','250','','','','0','0','1','0','1','1','1','100','1','1533092553','1533092553'), ('18','ismake','0','','是否静态页面','switch','tinyint(1)','250','','','','0','0','1','0','1','1','1','100','1','1533092698','1533092698'), ('19','seo_title','0','','SEO标题','text','varchar(250)','250','','','','0','0','1','0','1','1','1','100','1','1533092713','1533092713'), ('20','seo_keywords','0','','SEO关键词','text','varchar(250)','250','','','','0','0','1','0','1','1','1','100','1','1533092725','1533092725'), ('21','seo_description','0','','SEO描述','text','varchar(250)','250','','','','0','0','1','0','1','1','1','100','1','1533092739','1533092739'), ('22','status','0','','状态','switch','tinyint(1)','250','','','','0','0','1','0','1','1','1','100','1','1533092753','1533092753'), ('23','sort_order','0','','排序号','int','int(11)','250','','','','0','0','1','0','1','1','1','100','1','1533092766','1533092766'), ('27','content','6','','内容详情','htmltext','longtext','0','','','','0','0','1','0','0','0','0','1','1','1533464715','1598709529'), ('29','content','1','','内容详情','htmltext','longtext','0','','','','0','0','1','0','0','0','0','10','1','1533464713','1599103785'), ('30','update_time','-99','','更新时间','datetime','int(11)','11','0','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('31','add_time','-99','','新增时间','datetime','int(11)','11','0','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('32','status','-99','','启用 (1=正常，0=屏蔽)','switch','tinyint(1)','1','1','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('33','is_part','-99','','栏目属性：0=内容栏目，1=外部链接','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('34','is_hidden','-99','','是否隐藏栏目：0=显示，1=隐藏','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('35','sort_order','-99','','排序号','int','int(10)','10','0','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('36','seo_description','-99','','SEO描述','multitext','text','0','','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('37','seo_keywords','-99','','SEO关键字','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('38','seo_title','-99','','SEO标题','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('39','tempview','-99','','文档模板文件名','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('40','templist','-99','','列表模板文件名','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('41','litpic','-99','','栏目图片','img','varchar(250)','250','','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('42','typelink','-99','','栏目链接','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('43','grade','-99','','栏目等级','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('44','englist_name','-99','','栏目英文名','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('45','dirpath','-99','','目录存放HTML路径','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('46','dirname','-99','','目录英文名','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('47','typename','-99','','栏目名称','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('48','parent_id','-99','','栏目上级ID','int','int(10)','10','0','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('49','current_channel','-99','','栏目当前模型ID','int','int(10)','10','0','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('50','channeltype','-99','','栏目顶级模型ID','int','int(10)','10','0','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('51','id','-99','','栏目ID','int','int(10)','10','','','','0','0','1','0','1','1','1','100','1','1533524780','1533524780'), ('52','del_method','-99','','伪删除状态，1为主动删除，2为跟随上级栏目被动删除','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1547890773','1547890773'), ('53','is_del','0','','是否伪删除','switch','tinyint(1)','250','','','','0','0','1','0','1','1','1','100','1','1547890773','1547890773'), ('54','del_method','0','','伪删除状态，1为主动删除，2为跟随上级栏目被动删除','switch','tinyint(1)','250','','','','0','0','1','0','1','1','1','100','1','1547890773','1547890773'), ('176','update_time','1','','更新时间','datetime','int(11)','11','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('177','add_time','1','','新增时间','datetime','int(11)','11','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('178','del_method','1','','伪删除状态，1为主动删除，2为跟随上级栏目被动删除','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('179','is_del','1','','伪删除，1=是，0=否','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('180','admin_id','1','','管理员ID','int','int(10)','10','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('182','sort_order','1','','排序号','int','int(10)','10','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('183','status','1','','状态(0=屏蔽，1=正常)','switch','tinyint(1)','1','1','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('184','tempview','1','','文档模板文件名','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('187','seo_description','1','','SEO描述','multitext','text','0','','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('188','seo_keywords','1','','SEO关键词','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('189','seo_title','1','','SEO标题','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('190','ismake','1','','是否静态页面（0=动态，1=静态）','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('191','jumplinks','1','','外链跳转','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('192','arcrank','1','','阅读权限：0=开放浏览，-1=待审核稿件','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('193','click','1','','浏览量','int','int(10)','10','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('194','author','1','','作者','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('195','is_litpic','1','','图片（0=否，1=是）','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('196','is_jump','1','','跳转链接（0=否，1=是）','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('197','is_recom','1','','推荐（0=否，1=是）','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('198','is_top','1','','置顶（0=否，1=是）','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('199','is_special','1','','特荐（0=否，1=是）','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('200','is_head','1','','头条（0=否，1=是）','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('201','litpic','1','','缩略图','img','varchar(250)','250','','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('202','title','1','','标题','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('203','is_b','1','','加粗','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('204','channel','1','','模型ID','int','int(10)','10','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('205','typeid','1','','当前栏目','int','int(10)','10','0','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('206','aid','1','','aid','int','int(10)','10','','','','0','0','1','0','1','1','1','100','1','1562315416','1562315416'), ('326','joinaid','1','','关联文档id','int','int(10) unsigned','10','0','','','0','0','1','0','1','1','1','100','1','1568962806','1568962806'), ('425','is_del','-99','','伪删除，1=是，0=否','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1569036451','1569036451'), ('426','admin_id','-99','','管理员ID','int','int(10)','10','0','','','0','0','1','0','1','1','1','100','1','1569036451','1569036451'), ('467','update_time','6','','更新时间','datetime','int(11)','11','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('468','add_time','6','','新增时间','datetime','int(11)','11','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('469','del_method','6','','伪删除状态，1为主动删除，2为跟随上级栏目被动删除','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('470','is_del','6','','伪删除，1=是，0=否','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('471','admin_id','6','','管理员ID','int','int(10)','10','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('472','sort_order','6','','排序号','int','int(10)','10','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('473','status','6','','状态(0=屏蔽，1=正常)','switch','tinyint(1)','1','1','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('474','tempview','6','','文档模板文件名','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('477','seo_description','6','','SEO描述','multitext','text','0','','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('478','seo_keywords','6','','SEO关键词','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('479','seo_title','6','','SEO标题','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('480','ismake','6','','是否静态页面（0=动态，1=静态）','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('481','jumplinks','6','','外链跳转','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('482','arcrank','6','','阅读权限：0=开放浏览，-1=待审核稿件','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('483','click','6','','浏览量','int','int(10)','10','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('484','author','6','','作者','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('485','is_litpic','6','','图片（0=否，1=是）','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('486','is_jump','6','','跳转链接（0=否，1=是）','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('487','is_recom','6','','推荐（0=否，1=是）','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429');
INSERT INTO `pcf_channelfield` VALUES ('488','is_top','6','','置顶（0=否，1=是）','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('489','is_special','6','','特荐（0=否，1=是）','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('490','is_head','6','','头条（0=否，1=是）','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('491','litpic','6','','缩略图','img','varchar(250)','250','','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('492','title','6','','标题','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('493','is_b','6','','加粗','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('494','channel','6','','模型ID','int','int(10)','10','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('495','typeid','6','','当前栏目','int','int(10)','10','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('496','joinaid','6','','关联文档id','int','int(10) unsigned','10','0','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('497','aid','6','','aid','int','int(10)','10','','','','0','0','1','0','1','1','1','100','1','1569576429','1569576429'), ('521','admin_id','0','','管理员ID','int','int(10)','10','0','','','0','0','1','0','1','1','1','100','1','1570500751','1570500751'), ('522','tempview','0','','文档模板文件名','text','varchar(200)','200','','','','0','0','1','0','1','1','1','100','1','1570500751','1570500751'), ('523','is_litpic','0','','图片（0=否，1=是）','switch','tinyint(1)','1','0','','','0','0','1','0','1','1','1','100','1','1570500751','1570500751'), ('524','joinaid','0','','关联文档id','int','int(10) unsigned','10','0','','','0','0','1','0','1','1','1','100','1','1570500751','1570500751');
INSERT INTO `pcf_channelfield_bind` VALUES ('110','0','880','1','1585639563','1585639563'), ('139','0','879','1','1588685561','1588685561'), ('154','0','1254','1','1588985796','1588985796'), ('159','0','1290','1','1589176953','1589176953'), ('160','0','1289','1','1589177010','1589177010'), ('163','0','625','1','1595380918','1595380918'), ('164','0','626','1','1595382164','1595382164'), ('165','0','627','1','1595382351','1595382351'), ('166','0','628','1','1595409500','1595409500'), ('171','0','632','1','1595767065','1595767065'), ('172','0','633','1','1595767084','1595767084'), ('174','0','631','1','1595767309','1595767309'), ('175','0','635','1','1595767431','1595767431'), ('177','0','636','1','1595820425','1595820425'), ('178','0','637','1','1595908005','1595908005'), ('179','0','634','1','1595908063','1595908063'), ('186','0','644','1','1597419593','1597419593'), ('188','0','645','1','1597549895','1597549895'), ('193','0','646','1','1597650472','1597650472'), ('199','0','651','1','1597842643','1597842643'), ('203','0','27','1','1598408924','1598408924'), ('212','0','29','1','1599103785','1599103785');
INSERT INTO `pcf_channel_type` VALUES ('1','article','资讯模型','资讯','article','Article','1','1','1','0','20','1597648508','1597452593'), ('6','single','单页模型','单页','single','Single','1','1','1','0','10','1597648513','1597452590');
INSERT INTO `pcf_common_pic` VALUES ('55','4c4e7fd6eb316c5519a7236e184713ad','/uploads/allimg/20200828/f9ac7991a2e4f9fa4c8e41c9e4bbb12e.jpg','1598596796','1598596796'), ('58','d0fef4cea22761f16a19bdea1074db1e','/uploads/allimg/20200828/af128d99c336de1a6c67d0c0d0a75afe.jpg','1598709311','1598709311'), ('63','e50611f889cbb6fd4dd08c25bcd4f5c7','/uploads/allimg/20200831/17aa407e732920e65fdefee7c991f44d.jpg','1598853671','1598853671'), ('66','d1a6ce7105e675c5335be2a4e1a6caef','/uploads/allimg/20200831/1abc16e76d69db6f3a37d68bdb573260.jpg','1598873301','1598873301'), ('67','3e4d2bd16054db458ca7d66449da654f','/uploads/allimg/20200828/fd9cb51fdd7a1771acaabdf256431582.jpg','1598873813','1598873813');
INSERT INTO `pcf_config` VALUES ('1','is_mark','0','water','','0','1597452891','0','cn'), ('2','mark_txt','PCFCMS','water','','0','1588057217','0','cn'), ('3','mark_img','','water','','0','1586694527','0','cn'), ('4','mark_width','200','water','','0','0','0','cn'), ('5','mark_height','200','water','','0','1578880113','0','cn'), ('6','mark_degree','35','water','','0','1590631124','0','cn'), ('7','mark_quality','34','water','','0','1582942229','0','cn'), ('8','mark_sel','5','water','','0','1588057130','0','cn'), ('11','file_size','100','basic','','0','1595988506','0','cn'), ('12','image_type','jpg|gif|png|bmp|jpeg|ico','basic','','0','1578963547','0','cn'), ('13','file_type','zip|gz|rar|iso|doc|xsl|ppt|wps','basic','','0','0','0','cn'), ('14','media_type','swf|mpg|mp3|rm|rmvb|wmv|wma|wav|mid|mov|mp4','basic','','0','0','0','cn'), ('15','web_keywords','五金紧固件机械设备有限公司','web','','0','1598594437','0','cn'), ('18','seo_viewtitle_format','3','seo','','0','1590808402','0','cn'), ('24','mark_type','text','water','','0','1590631130','0','cn'), ('25','mark_txt_size','30','water','','0','0','0','cn'), ('26','mark_txt_color','#852020','water','','0','1588057599','0','cn'), ('28','web_name','pcfcms_网站开发','web','','0','1598939806','0','cn'), ('29','web_logo','','web','','0','1599205299','0','cn'), ('30','web_ico','/favicon.ico','web','','0','1568962123','0','cn'), ('31','web_basehost','','web','','0','1596028889','0','cn'), ('32','web_description','五金紧固件机械设备有限公司','web','','0','1598594437','0','cn'), ('33','web_copyright','','web','','0','1596081977','0','cn'), ('34','web_thirdcode_pc','','web','','0','0','0','cn'), ('35','web_thirdcode_wap','','web','','0','0','0','cn'), ('39','seo_arcdir','','seo','','0','0','0','cn'), ('40','seo_pseudo','3','seo','','0','1598167682','0','cn'), ('41','list_symbol',' > ','basic','','0','1578641191','0','cn'), ('42','sitemap_auto','1','sitemap','','0','1593694861','0','cn'), ('47','sitemap_zzbaidutoken','','sitemap','','0','1596073835','0','cn'), ('48','seo_expires_in','7200','seo','','0','0','0','cn'), ('55','web_title','五金紧固件机械设备有限公司','web','','0','1598594437','0','cn'), ('57','web_authortoken','','web','','0','0','0','cn'), ('62','seo_inlet','1','seo','','0','1569719545','0','cn'), ('63','web_cmspath','','web','','0','1596786903','0','cn'), ('64','web_sqldatapath','/backup/sqldata/','web','','0','0','0','cn'), ('65','web_cmsurl','','web','','0','1586000604','0','cn'), ('66','web_templets_dir','/template/default','web','','0','1588835254','0','cn'), ('67','web_templeturl','/template/default','web','','0','1588835254','0','cn'), ('68','web_templets_pc','/template/default/pc','web','','0','1588835254','0','cn'), ('69','web_templets_m','/template/default/mobile','web','','0','1588835254','0','cn'), ('70','web_pcfcms','http://www.pcfcms.com','web','','0','1595841028','0','cn'), ('76','seo_liststitle_format','1','seo','','0','1590808629','0','cn'), ('77','web_status','1','web','','0','1596789811','0','cn'), ('79','web_recordnum','粤ICP备xxxxxxxx号','web','','0','1598596094','0','cn'), ('80','web_is_authortoken','-1','web','','0','1596176181','0','cn'), ('81','web_adminbasefile','/login.php','web','','0','1596787846','0','cn'), ('82','seo_rewrite_format','2','seo','','0','1598450378','0','cn'), ('88','seo_dynamic_format','1','seo','','0','0','0','cn'), ('89','system_sql_mode','NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION','system','','0','1570841249','0','cn'), ('174','web_is_https','0','web','','0','1578966774','0','cn'), ('190','system_auth_code','FBWQg3f7Psur9E6Usp8j','system','','0','1557462848','0','cn'), ('192','system_upgrade_filelist','','system','','0','1591863508','0','cn'), ('193','system_version','v2.1.0','system','','0','1592050685','0','cn'), ('197','seo_html_arcdir_limit','/app,/backup,/config,/extend,/public/install,/public,/public/template,/public/uploads,/vendor','seo','','0','1561456822','0','cn'), ('198','seo_html_arcdir','','seo','','0','1584950558','0','cn'), ('199','seo_html_listname','1','seo','','0','1561710967','0','cn'), ('200','seo_html_pagename','1','seo','','0','1561389643','0','cn'), ('201','seo_force_inlet','1','seo','','0','1570782456','0','cn'), ('207','system_correctarctypedirpath','1','system','','0','1562052310','0','cn'), ('209','thumb_open','0','thumb','','0','1585645546','0','cn'), ('210','thumb_mode','3','thumb','','0','1585555509','0','cn'), ('211','thumb_color','#FFFFFF','thumb','','0','1562313846','0','cn'), ('212','thumb_width','300','thumb','','0','1562313846','0','cn'), ('213','thumb_height','300','thumb','','0','1562313846','0','cn'), ('220','web_templets_mobile','/template/default/mobile','web','','0','0','0','cn'), ('232','file','','web','','0','1568962123','0','cn'), ('234','basic_indexname','网站首页','basic','','0','1578641388','0','cn'), ('235','max_filesize','100','basic','','0','1578641191','0','cn'), ('236','max_sizeunit','MB','basic','','0','1568967416','0','cn'), ('238','system_tpl_theme','default','system','','0','1589291339','0','cn'), ('241','web_mobile_domain_open','0','web','','0','1598593772','0','cn'), ('242','web_mobile_domain','m','web','','0','1588491084','0','cn'), ('245','web_adminlogo','/admin/assets/images/logo.png','web','','0','1582614294','0','cn'), ('246','old_web_adminlogo','/admin/assets/images/logo.png','web','','0','1582614323','0','cn'), ('252','system_channeltype_unit','1','system','','0','1572340836','0','cn'), ('262','is_thumb_mark','0','thumb','','0','1588057488','0','cn'), ('266','sitemap_archives_num','100','sitemap','','0','1593671025','0','cn'), ('270','file','','water','','0','1579241157','0','cn'), ('271','is_thumb_mark','0','thumb','','0','1588057488','0','cn'), ('274','web_cmsmode','0','web','','0','1586673690','0','cn'), ('275','web_show_popup_upgrade','1','web','','0','1598098010','0','cn'), ('278','smtp_syn_weapp','1','smtp','','0','1593397818','0','cn'), ('279','smtp_server','smtp.qq.com','smtp','','0','0','0','cn'), ('280','smtp_port','465','smtp','','0','1590457867','0','cn'), ('281','smtp_user','pcfcms@qq.com','smtp','','0','1599119096','0','cn'), ('282','smtp_pwd','123456','smtp','','0','1599119096','0','cn'), ('283','smtp_test','','smtp','','0','1593327886','0','cn'), ('284','question_status','1','question','','0','0','0','cn'), ('285','question_ask_status','1','question','','0','0','0','cn'), ('286','question_ask_check','0','question','','0','1591585393','0','cn'), ('287','question_ans_status','1','question','','0','0','0','cn'), ('288','question_ans_check','0','question','','0','1591585393','0','cn'), ('289','question_typename','问答中心','question','','0','1591585425','0','cn'), ('294','sms_syn_weapp','1','sms','','0','1593402919','0','cn'), ('295','sms_appkey','pcfcms','sms','','0','1595988941','0','cn'), ('296','sms_secretkey','123456789','sms','','0','1596028924','0','cn'), ('297','sms_test','','sms','','0','1595988925','0','cn'), ('302','sms_content','','sms','','0','1593333167','0','cn'), ('303','wxpay_syn_weapp','1','wxpay','','0','0','0','cn');
INSERT INTO `pcf_config` VALUES ('304','wxpay_appid','pcfcms','wxpay','','0','1595988955','0','cn'), ('305','wxpay_sh','123456789','wxpay','','0','1596028936','0','cn'), ('306','wxpay_appkey','123456789','wxpay','','0','1596028936','0','cn'), ('311','qq_syn_weapp','1','qq','','0','0','0','cn'), ('312','qq_appid','pcfcms','qq','','0','1595988961','0','cn'), ('313','qq_appkey','123456789','qq','','0','1596028943','0','cn'), ('314','qq_huitiao',NULL,'qq','','0','0','0','cn'), ('315','sms_platform','1','sms','','0','1593398760','0','cn'), ('316','qq_appid','pcfcms','qq','','0','1595988961','0','cn'), ('317','qq_appkey','123456789','qq','','0','1596028943','0','cn'), ('318','qq_huitiao','','qq','','0','1593588798','0','cn'), ('319','sitemap_xml','1','sitemap','','0','1593670987','0','cn'), ('321','oss_switch','0','oss','','0','0','0','cn'), ('322','adminbasefile_old','login','web','','0','1597578251','0','cn'), ('323','adminbasefile','login','web','','0','1597578251','0','cn');
INSERT INTO `pcf_field_type` VALUES ('checkbox','多选项','1','5','1532485708','1532485708'), ('datetime','日期和时间','0','12','1532485708','1532485708'), ('decimal','金额类型','0','9','1532485708','1532485708'), ('float','小数类型','0','8','1532485708','1532485708'), ('htmltext','HTML文本','0','3','1532485708','1532485708'), ('img','单张图','0','10','1532485708','1532485708'), ('imgs','多张图','0','11','1532485708','1532485708'), ('int','整数类型','0','7','1532485708','1532485708'), ('multitext','多行文本','0','2','1532485708','1532485708'), ('radio','单选项','1','4','1532485708','1532485708'), ('select','下拉框','1','6','1532485708','1532485708'), ('switch','开关','0','13','1532485708','1532485708'), ('text','单行文本','0','1','1532485708','1532485708');
INSERT INTO `pcf_guestbook` VALUES ('10','在线留言','0','1','1598880028','1598881463','cn');
INSERT INTO `pcf_guestbook_attr` VALUES ('41','你的姓名','10','text','','100','1','0','1598880028','0','cn'), ('42','手机号码','10','text','','100','1','0','1598880028','0','cn'), ('43','邮箱号码','10','text','','100','1','0','1598880028','0','cn'), ('44','留言内容','10','multitext','','100','1','0','1598880028','0','cn');
INSERT INTO `pcf_links` VALUES ('17','1','pcfcms','http://www.pcfcms.com','','1','1','','','1','1595314307','1598409156','cn'), ('18','1','网站建设','http://www.gzpcfweb.com','','3','1','','','1','1595314341','1598409162','cn'), ('20','1','免费模板','http://www.pcfcms88.com','','2','1','','','1','1595314392','1598409160','cn');
INSERT INTO `pcf_menu` VALUES ('1','0','系统','layui-icon-set','/index/main','','','1','1','1586691761','1597422077','cn','1','1'), ('2','0','内容','layui-icon-read','/Content/index','','文档列表','1','2','1586691778','1597658736','cn','1','1'), ('3','0','会员','layui-icon-user','/users.Index/index','','会员列表','1','3','1586691792','1597423723','cn','1','1'), ('4','1','系统配置','layui-icon-set-fill','','','','2','2','1586695887','1587886250','cn','1','1'), ('5','4','网站信息','','/system.Web/index','system.Web/index','','3','1','1586696470','1586747289','cn','1','1'), ('6','5','查看','','','','list','4','10','1586698676','1586832699','cn','1','1'), ('7','5','修改','','','','modify','4','10','1586698981','1586832712','cn','1','1'), ('8','5','删除','','','','delete','4','10','1586699004','1586832724','cn','1','1'), ('9','4','图片处理','','/system.Water/index','system.Water/index','','3','2','1586700768','1586750781','cn','1','1'), ('10','4','数据备份','','/system.Tools/index','system.Tools/index','','3','3','1586700790','1586750778','cn','1','1'), ('11','4','定时任务','','/Task/index','Task/index','','3','4','1586700833','1586847964','cn','1','1'), ('12','4','程序升级','','/Update/index','Update/index','','3','5','1586700856','1588396798','cn','1','1'), ('13','1','权限管理','layui-icon-auz','','','','2','3','1586700909','1588409378','cn','1','1'), ('14','13','节点管理','','/system.Menu/index','system.Menu/index','','3','1','1586700974','1586747425','cn','1','1'), ('15','13','管理员','','/system.Admin/index','system.Admin/index','','3','2','1586701010','1586747435','cn','1','1'), ('16','13','角色管理','','/system.Authrole/index','system.Authrole/index','','3','3','1586701032','1586747444','cn','1','1'), ('17','2','内容管理','layui-icon-file-b','','','','2','1','1586701249','1586703979','cn','1','1'), ('18','2','扩展功能','layui-icon-app','','','','2','2','1586701321','1586703998','cn','1','1'), ('19','3','会员管理','layui-icon-username','','','','2','1','1586701436','1586704022','cn','1','1'), ('21','17','导航管理','','/Nav/index','Nav/index','','3','2','1586702099','1596784726','cn','1','1'), ('22','18','模型管理','','/channel.Type/index','channel.Type/index','','3','4','1586702124','1588573866','cn','1','1'), ('23','17','数据回收','','/RecycleBin/index','RecycleBin/index','','3','255','1586702200','1593596819','cn','1','1'), ('24','18','广告管理','','/channel.Adposition/index','channel.Adposition/index','','3','3','1586702242','1589955954','cn','1','1'), ('25','18','友情链接','','/channel.Links/index','channel.Links/index','','3','3','1586702256','1589955911','cn','1','1'), ('26','18','表单功能','','/Guestbook/index','Guestbook/index','','3','3','1586702273','1586747593','cn','1','1'), ('27','18','主题模板','','/channel.Filemanager/index','channel.Filemanager/index','','3','4','1586702288','1598519897','cn','0','1'), ('28','18','URL配置','','/channel.Seo/index','channel.Seo/index','','3','5','1586702305','1598519898','cn','0','1'), ('29','18','网站地图','','/channel.Sitemap/index','channel.Sitemap/index','','3','6','1586702321','1593596661','cn','1','1'), ('30','4','资源管理','','/Res/index','Res/index','','3','6','1586702462','1596789268','cn','1','1'), ('31','19','会员列表','','/users.Index/index','users.Index/index','','3','1','1586702532','1586747632','cn','1','1'), ('32','19','会员等级','','/users.Userslevel/index','users.Userslevel/index','','3','2','1586703709','1586754730','cn','1','1'), ('34','19','会员配置','','/users.Usersconfig/index','users.Usersconfig/index','','3','4','1586703754','1588918421','cn','1','1'), ('36','9','查看','','','','list','4','10','1586744704','1586839229','cn','1','1'), ('37','9','修改','','','','modify','4','10','1586744726','1586839244','cn','1','1'), ('38','10','查看','','','','list','4','10','1586745064','0','cn','1','1'), ('39','10','备份','','','','export','4','10','1586745081','1588395699','cn','1','1'), ('40','10','删除','','','','delete','4','10','1586745090','0','cn','1','1'), ('41','11','查看','','','','list','4','10','1586745120','1586847972','cn','1','1'), ('42','11','修改','','','','modify','4','10','1586745131','1586847972','cn','1','1'), ('43','11','删除','','','','delete','4','10','1586745143','1586847973','cn','1','1'), ('44','12','查看','','','','list','4','10','1586745201','1586847976','cn','1','1'), ('45','12','修改','','','','modify','4','10','1586745216','1586847977','cn','1','1'), ('46','30','查看','','','','list','4','10','1586745318','1596789269','cn','1','1'), ('48','30','删除','','','','delete','4','10','1586745342','1596789270','cn','1','1'), ('50','14','查看','','','','list','4','10','1586745391','0','cn','1','1'), ('51','14','修改','','','','modify','4','10','1586745402','0','cn','1','1'), ('52','14','删除','','','','delete','4','10','1586745417','0','cn','1','1'), ('53','14','新增','','','','add','4','10','1586745433','0','cn','1','1'), ('54','15','查看','','','','list','4','10','1586745452','0','cn','1','1'), ('55','15','修改','','','','modify','4','10','1586745463','0','cn','1','1'), ('56','15','删除','','','','delete','4','10','1586745475','0','cn','1','1'), ('57','15','新增','','','','add','4','10','1586745489','0','cn','1','1'), ('58','16','查看','','','','list','4','10','1586745508','1586747072','cn','1','1'), ('59','16','修改','','','','modify','4','10','1586745520','1586747095','cn','1','1'), ('60','16','删除','','','','delete','4','10','1586745533','1586747108','cn','1','1'), ('61','16','新增','','','','add','4','10','1586745546','1586747121','cn','1','1'), ('66','17','分类管理','','/Arctype/index','Arctype/index','','3','3','1586745864','1593446475','cn','1','1'), ('67','66','查看','','','','list','4','10','1586745929','0','cn','1','1'), ('68','66','修改','','','','modify','4','10','1586745939','0','cn','1','1'), ('69','66','删除','','','','delete','4','10','1586745951','0','cn','1','1'), ('70','66','新增','','','','add','4','10','1586745962','0','cn','1','1'), ('71','21','查看','','','','list','4','10','1586746060','1596784728','cn','1','1'), ('72','21','修改','','','','modify','4','10','1586746072','1596784728','cn','1','1'), ('73','21','删除','','','','delete','4','10','1586746085','1596784729','cn','1','1'), ('74','21','新增','','','','add','4','10','1586746098','1596784729','cn','1','1'), ('75','22','查看','','','','list','4','10','1586746116','0','cn','1','1'), ('76','22','修改','','','','modify','4','10','1586746127','0','cn','1','1'), ('77','22','删除','','','','delete','4','10','1586746140','0','cn','1','1'), ('78','22','新增','','','','add','4','10','1586746160','1598951320','cn','1','1'), ('79','23','查看','','','','list','4','10','1586746180','0','cn','1','1'), ('80','23','删除','','','','delete','4','10','1586746194','0','cn','1','1'), ('81','24','查看','','','','list','4','10','1586746254','1586832574','cn','1','1'), ('82','24','修改','','','','modify','4','10','1586746264','1586832607','cn','1','1'), ('83','24','删除','','','','delete','4','10','1586746274','1586832625','cn','1','1'), ('84','24','新增','','','','add','4','10','1586746292','1586832641','cn','1','1'), ('85','25','查看','','','','list','4','10','1586746303','0','cn','1','1'), ('86','25','修改','','','','modify','4','10','1586746313','0','cn','1','1'), ('87','25','删除','','','','delete','4','10','1586746327','0','cn','1','1'), ('88','25','新增','','','','add','4','10','1586746343','0','cn','1','1'), ('89','26','查看','','','','list','4','10','1586746360','1586786337','cn','1','1'), ('90','26','修改','','','','modify','4','10','1586746371','1586786337','cn','1','1'), ('91','26','删除','','','','delete','4','10','1586746381','1586786338','cn','1','1'), ('92','26','新增','','','','add','4','10','1586746398','1586786338','cn','1','1'), ('93','27','查看','','','','list','4','10','1586746421','1598519907','cn','1','1'), ('94','27','修改','','','','modify','4','10','1586746431','1598519905','cn','1','1'), ('95','27','删除','','','','delete','4','10','1586746442','1598519906','cn','1','1'), ('96','27','新增','','','','add','4','10','1586746455','1598519904','cn','1','1'), ('97','28','查看','','','','list','4','10','1586746500','1598450096','cn','1','1'), ('98','28','修改','','','','modify','4','10','1586746516','1598450097','cn','1','1'), ('99','29','查看','','','','list','4','10','1586746607','1596079856','cn','1','1'), ('100','29','修改','','','','modify','4','10','1586746622','1596079857','cn','1','1'), ('101','31','查看','','','','list','4','10','1586746639','0','cn','1','1'), ('102','31','修改','','','','modify','4','10','1586746648','0','cn','1','1'), ('103','31','删除','','','','delete','4','10','1586746657','0','cn','1','1'), ('104','31','新增','','','','add','4','10','1586746667','0','cn','1','1'), ('105','32','查看','','','','list','4','10','1586746681','0','cn','1','1'), ('106','32','修改','','','','modify','4','10','1586746691','0','cn','1','1'), ('107','32','删除','','','','delete','4','10','1586746703','0','cn','1','1'), ('108','32','新增','','','','add','4','10','1586746714','0','cn','1','1'), ('113','34','查看','','','','list','4','10','1586746779','0','cn','1','1');
INSERT INTO `pcf_menu` VALUES ('114','34','修改','','','','modify','4','10','1586746790','0','cn','1','1'), ('118','1','后台首页','layui-icon-home','/index/main','index/main','','2','1','1586754893','1588409473','cn','1','1'), ('145','5','新增','','','','add','4','10','1586837276','1586848793','cn','1','1'), ('146','14','状态','','','','status','4','10','1587814987','0','cn','1','1'), ('147','15','状态','','','','status','4','10','1587894599','0','cn','1','1'), ('148','15','个人中心','','','','userinfo','4','10','1587954221','1588397304','cn','1','1'), ('149','23','还原','','','','databack','4','10','1588153520','0','cn','1','1'), ('150','10','优化','','','','optimze','4','10','1588395730','0','cn','1','1'), ('151','10','修复','','','','repair','4','10','1588395759','0','cn','1','1'), ('152','10','恢复','','','','import','4','10','1588395843','0','cn','1','1'), ('153','10','下载','','','','down','4','10','1588395864','0','cn','1','1'), ('154','11','添加','','','','add','4','10','1588396837','0','cn','1','1'), ('155','26','状态','','','','status','4','10','1588420041','0','cn','1','1'), ('156','25','状态','','','','status','4','10','1588423866','0','cn','1','1'), ('157','22','状态','','','','status','4','10','1588667166','0','cn','1','1'), ('158','17','文档列表','','/Content/index','Content/index','','3','1','1588670423','1597658690','cn','1','1'), ('159','158','查看','','','','list','4','10','1588670564','0','cn','1','1'), ('160','158','修改','','','','modify','4','10','1588670585','0','cn','1','1'), ('161','158','删除','','','','delete','4','10','1588670636','0','cn','1','1'), ('162','158','新增','','','','add','4','10','1588670669','0','cn','1','1'), ('163','158','状态','','','','status','4','10','1588670689','0','cn','1','1'), ('164','15','清空日志','','','','dellog','4','10','1588730009','0','cn','1','1'), ('177','31','状态','','','','status','4','10','1591777892','0','cn','1','1'), ('178','32','状态','','','','status','4','10','1591777909','0','cn','1','1'), ('203','66','查看单页','','','','chakan','4','10','1596089426','1596089446','cn','1','1');
INSERT INTO `pcf_nav_position` VALUES ('1','PC端主导航','100','0','0','0'), ('4','PC端底部导航','100','0','0','0'), ('5','WAP端主导航','100','0','0','0'), ('6','WAP端底部导航','100','0','0','0');
INSERT INTO `pcf_quickentry` VALUES ('3','资讯','资讯列表','1','Article','index','channel=1','1','0',NULL,'1','6','1569232484','1599206729'), ('5','单页','单页列表','1','Arctype','single_index','channel=6','1','0',NULL,'1','8','1569232484','1599206729'), ('7','回收站','回收站','1','RecycleBin','index','','0','0','2','0','11','1569232484','1585042469'), ('8','分类管理','分类管理','1','Arctype','index','','0','1','2','1','5','1569232484','1599046858'), ('10','网站信息','网站信息','1','Web','index','','0','1','1','1','1','1569232484','1599046858'), ('11','图片处理','图片处理','1','Water','index','','0','1','1','1','2','1569232484','1599046858'), ('13','数据备份','数据备份','1','Tools','index','','0','1','1','1','3','1569232484','1599046858'), ('14','URL配置','URL配置','1','Seo','index','','0','0',NULL,'0','10','1569232484','1598863614'), ('15','主题模板','主题模板','1','Filemanager','index','','0','0',NULL,'0','9','1569232484','1598863614'), ('16','SiteMap','SiteMap','1','Seo','index','inc_type=sitemap','0','0',NULL,'0','12','1569232484','1585042469'), ('17','频道模型','频道模型','1','Type','index','','0','1',NULL,'1','6','1569232484','1599046858'), ('18','广告管理','广告管理','1','Adposition','index','','0','1',NULL,'1','7','1569232484','1599046858'), ('19','友情链接','友情链接','1','Links','index','','0','1',NULL,'1','8','1569232484','1599046858'), ('21','管理员','管理员','1','Admin','index','','0','1','1','1','4','1569232484','1599046858'), ('23','资讯','资讯列表','2','Article','index','channel=1','1','1','2','1','1','1569310798','1599206729'), ('28','广告','广告管理','2','Adposition','index','','0','1','1','0','5','1569232484','1583227448'), ('29','友情链接','友情链接','2','Links','index','','0','1','1','0','4','1569232484','1583227448');
INSERT INTO `pcf_update_config` VALUES ('7','server','aHR0cDovL3d3dy5wY2ZjbXMuY29tLw==','','update','1598923314','cn'), ('8','ip','','','update','1598923314','cn'), ('9','onlyid','cGNmY21z','','update','1598923314','cn'), ('10','date','','','update','1598923314','cn'), ('11','status','1','','update','1598923314','cn');
INSERT INTO `pcf_users_config` VALUES ('1','users_reg_notallow','www,bbs,ftp,mail,user,users,admin,administrator,pcfcms','不允许注册的会员名','users','1599098414','cn'), ('2','users_verification','0','验证方式','users','1599098414','cn'), ('3','users_open','1','开放会员','users','1599098414','cn'), ('4','users_open_reg','1','开放注册','users','1599098414','cn'), ('5','users_open_login','1','开放登陆','users','1599098414','cn'), ('6','userspass_verification','0','找回密码','users','1599098414','cn');
INSERT INTO `pcf_users_level` VALUES ('1','高级会员','0','0','0','0','1','1598967485','0','0','0','cn'), ('2','普通会员','0','0','0','0','1','1598967494','0','0','0','cn');
