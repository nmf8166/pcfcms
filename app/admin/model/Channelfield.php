<?php
/***********************************************************
 * 模型字段模型
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Session;
use think\facade\Request;
class Channelfield extends Common
{
    //字段列表
    public function tableData($post)
    {
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $tableWhere = $this->pcftableWhere($post);
        $list = Db::name('channelfield')->field($tableWhere['field'])->where($tableWhere['where'])->order($tableWhere['order'])->paginate($limit);
        $data = $this->tableFormat($list->items());
        foreach ($data as $key => $value) {
            $data[$key]['pcfchannel_id'] = $post['channel_id'];
            $data[$key]['dtypename'] = Db::name('field_type')->where('name', $value['dtype'])->value('title');
            $data[$key]['add_time'] = pcftime($value['add_time']);
            $data[$key]['update_time'] = pcftime($value['update_time']);
        }
        $result = ['code' => 0, 'msg' => 'ok','count' =>$list->total(),'data' => $data];
        return $result;
    }

    protected function pcftableWhere($post)
    {
        $where = [];
        if(!empty($post['channel_id']) && $post['channel_id'] > 0){
            $where[] = ['channel_id', '=', $post['channel_id']];
        }
        $where[] = ['ifcontrol', '=', 0];//为了不显示系统内置字段
        $result['where'] = $where;
        $result['field'] = "*";
        $result['order'] = "sort_order asc, ifmain asc, ifcontrol asc, id desc";
        return $result;
    }

}
