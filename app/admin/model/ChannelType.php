<?php
/***********************************************************
 * 频道模型
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\model;
use app\admin\model\FieldType;
use think\facade\Db;
use think\facade\Request;
use think\facade\Cache;

class ChannelType extends Common
{
    //列表
    public function tableData($post)
    {
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $list = Db::name('channel_type')->order('sort_order asc')->paginate($limit);
        $data = $this->tableFormat($list->getCollection());
        $newdata = array();
        foreach ($data as $key => $value) {
            $newdata[$key]['id'] = $value['id'];
            $newdata[$key]['nid'] = $value['nid'];
            $newdata[$key]['title'] = $value['title'];
            $newdata[$key]['ifsystem'] = $value['ifsystem'];
            $newdata[$key]['sort_order'] = $value['sort_order'];
            $newdata[$key]['status'] = $value['status'];
        }
        $result = ['code' => 0, 'msg' => 'ok','count' =>$list->total(),'data' => $newdata];
        return $result;
    }

    //添加|编辑
    public function toAdd($data,$channeltype_system_nid)
    {
        $result = array('status' => false,'data' => '','msg' => '','url' =>'');
        $domain = Request::baseFile().'/channel.type/index';
        //判断是新增还是修改
        if (isset($data['id']) && !empty($data['id'])) {
            $where = [];
            $where[] = ['nid','=',$data['nid']];
            $where[] = ['id','<>',$data['id']];
            $edit_data = array();
            if (empty($data['ntitle'])) {
                $result['status'] = false;
                $result['msg'] = "模型名称不能为空！";
                return $result;
            }
            if(Db::name('channel_type')->where($where)->count('id') > 0){
                $result['status'] = false;
                $result['msg'] = "该模型标识已存在，请检查";
                $result['url'] = $domain;
                return $result;
            }
            $edit_data['nid'] = $data['nid'];
            $edit_data['title']  = $data['ntitle']."模型";
            $edit_data['ntitle']  = $data['ntitle'];
            $edit_data['table'] = $data['nid'];
            $edit_data['ctl_name'] = ucfirst($data['nid']);
            $edit_data['is_repeat_title'] = $data['is_repeat_title'];
            $edit_data['add_time'] = getTime();
            if (Db::name('channel_type')->where('id', $data['id'])->data($edit_data)->update()) {
                $result['msg']    = '修改成功';
                $result['status'] = true;
                $result['url'] = $domain;
                return $result;
            } else {
                $result['msg']    = '修改失败';
                $result['status'] = false;
                return $result;
            }
        } else {
            //判断用户名是否重复
            $manageInfo = Db::name('channel_type')->where('ntitle', $data['ntitle'])->find();
            $data['ntitle'] = trim($data['ntitle']);
            if (!$data['ntitle']) {
                $result['status'] = false;
                $result['msg'] = "模型名称不能为空！";
                return $result;
            }
            if (!$data['nid']) {
                $result['status'] = false;
                $result['msg'] = "输入模型标识";
                return $result;
            }else{
                if (!preg_match('/^([a-z]+)([a-z0-9]*)$/i', $data['nid'])) {
                    $result['status'] = false;
                    $result['msg'] = "模型标识必须以小写字母开头！";
                    return $result;
                } else if (in_array($data['nid'], $channeltype_system_nid)) {
                    $result['status'] = false;
                    $result['msg'] = "系统禁用当前模型标识，请更改！";
                    return $result;
                }
            }
            $post['ntitle'] = $data['ntitle'];
            $post['is_repeat_title'] = $data['is_repeat_title'];
            $post['nid']    = strtolower($data['nid']);
            $nid = $post['nid'];
            $post['ctl_name'] = ucwords($nid);
            $post['table']    = $nid;
            if ($manageInfo) {
                $result['status'] = false;
                $result['msg'] = "模型已存在";
                return $result;
            }
            //创建文件以及数据表
            $this->create_sql_file($post);
            $add_data = array(
                'title'        => $data['ntitle'].'模型',
                'ntitle'        => $data['ntitle'],
                'nid'           => $nid,
                'add_time'      => getTime(),
                'update_time'   => getTime(),
            );
            $add_data = array_merge($post, $add_data);         
            $insertId = Db::name('channel_type')->insertGetId($add_data);
            if ($insertId) {
                //复制模型字段基础数据
                $FieldType = new FieldType;
                $FieldType->synArchivesTableColumns($insertId,'');
                try {
                    schemaTable($data['table'].'_content'); //生成数据表字段信息缓存
                    $this->syn_custom_quickmenu($add_data, $insertId);//追加到快速入口列表
                } catch (\Exception $e) {}
                Cache::clear();//清除数据缓存文件
                $result['msg']    = '添加成功';
                $result['status'] = true;
                $result['url'] = $domain;
                return $result;
            } else {
                $result['msg']    = '添加失败';
                $result['status'] = false;
                return $result;
            }
        }
    }

    //同步自定义模型的快捷导航
    private function syn_custom_quickmenu($data = [], $insertId)
    {
        $saveData = [
            [
                'title' => $data['title'],
                'laytext'   => $data['title'].'列表',
                'type' => 1,
                'controller' => $data['table'],
                'action' => 'index',
                'vars' => 'channel='.$insertId,
                'sort_order' => 100,
                'groups'    => 1,
                'add_time' => getTime(),
                'update_time' => getTime(),
            ],
            [
                'title' => $data['title'],
                'laytext'   => $data['title'].'列表',
                'type' => 2,
                'controller' => $data['table'],
                'action' => 'index',
                'vars' => 'channel='.$insertId,
                'sort_order' => 100,
                'groups'    => 1,
                'add_time' => getTime(),
                'update_time' => getTime(),
            ],
        ];
        Db::name('quickentry')->insertAll($saveData);
    }

    // 创建文件以及数据表
    private function create_sql_file($post) 
    {
        $result = array('status' => false,'data' => '','msg' => '','url' =>'');
        $demopath = ROOT_PATH.'extend/model/';
        $fileArr = []; // 生成的相关文件记录
        $filelist = getDirFile($demopath);
        foreach ($filelist as $key => $file) {
            if (stristr($file, 'custom_model_path')) {
                unset($filelist[$key]);
                continue;
            }
            $src = $demopath.$file;
            if(0 == $key || 1 == $key){
                $dst = ROOT_PATH.$file;
            }else{
                $dst = $file;
            }

            $dst = str_replace('CustomModel', $post['ctl_name'], $dst);
            $dst = str_replace('custommodel', $post['nid'], $dst);
            /*记录相关文件*/
            if (!stristr($dst, 'custom_model_path')) {
                array_push($fileArr, $dst);
            }
            if(tp_mkdir(dirname($dst))) {
                $fileContent = @file_get_contents($src);
                $fileContent = str_replace('CustomModel', $post['ctl_name'], $fileContent);
                $fileContent = str_replace('custommodel', strtolower($post['nid']), $fileContent);
                $fileContent = str_replace('CUSTOMMODEL', strtoupper($post['nid']), $fileContent);
                $view_suffix = config('view.view_suffix');
                if (stristr($file, 'lists_custommodel.'.$view_suffix)) {
                    $replace = <<<EOF
<section class="article-list">
                    {gzpcf:list pagesize="10" titlelen="38"}
                    <article>
                        {gzpcf:notempty name="\$field.is_litpic"}
                        <a href="{\$field.arcurl}" title="{\$field.title}" style="float:left;margin-right:10px"> <img src="{\$field.litpic}" alt="{\$field.title}" height="100" /> </a>
                        {/gzpcf:notempty} 
                        <h2><a href="{\$field.arcurl}" class="">{\$field.title}</a><span>{\$field.click}°C</span></h2>
                        <div class="excerpt">
                            <p>{\$field.seo_description}</p>
                        </div>
                        <div class="meta">
                            <span class="item"><time>{\$field.add_time|MyDate='Y-m-d',###}</time></span>
                            <span class="item">{\$field.typename}</span>
                        </div>
                    </article>
                    {/gzpcf:list}
                </section>
                <section class="list-pager">
                    {gzpcf:pagelist listitem='index,pre,pageno,next,end' listsize='2'/}
                    <div class="clear"></div>
                </section>
EOF;
                    $fileContent = str_replace("<!-- #list# -->", $replace, $fileContent);
                }
                $puts = @file_put_contents($dst, $fileContent);
                if (!$puts) {
                    $result['msg']    = '创建自定义模型生成相关文件失败，请检查站点目录权限！';
                    $result['status'] = false;
                    return $result;
                }
            }
        }
        @file_put_contents($demopath.'custom_model_path/'.$post['nid'].'.filelist.txt', implode("\n\r", $fileArr));
        $table = config('database.connections.mysql.prefix').$post['table'].'_content';
        $tableSql = <<<EOF
CREATE TABLE `{$table}` (
  `id`          int(10) NOT NULL    AUTO_INCREMENT,
  `aid`         int(10) DEFAULT '0' COMMENT         '文档ID',
  `add_time`    int(11) DEFAULT '0' COMMENT         '新增时间',
  `update_time` int(11) DEFAULT '0' COMMENT         '更新时间',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='附加表';
EOF;
        $sqlFormat  = $this->sql_split($tableSql, config('database.connections.mysql.prefix'));
        //执行SQL语句
        try {
            $counts = count($sqlFormat);
            for ($i = 0; $i < $counts; $i++) {
                $sql = trim($sqlFormat[$i]);
                if (stristr($sql, 'CREATE TABLE')) {
                    Db::execute($sql);
                } else {
                    if(trim($sql) == ''){
                       continue;
                    }
                    Db::execute($sql);
                }
            }
        } catch (\Exception $e) {
            $result['msg']    = '创建文件以及数据表抛出异常！';
            $result['status'] = false;
            return $result;
        }
    }

    //解析sql语句
    private function sql_split($sql, $tablepre) 
    {  
        $sql = preg_replace("/TYPE=(InnoDB|MyISAM|MEMORY)( DEFAULT CHARSET=[^; ]+)?/", "ENGINE=\\1 DEFAULT CHARSET=utf8", $sql);
        $sql = str_replace("\r", "\n", $sql);
        $ret = array();
        $num = 0;
        $queriesarray = explode(";\n", trim($sql));
        unset($sql);
        foreach ($queriesarray as $query) {
            $ret[$num] = '';
            $queries = explode("\n", trim($query));
            $queries = array_filter($queries);
            foreach ($queries as $query) {
                $str1 = substr($query, 0, 1);
                if ($str1 != '#' && $str1 != '-')
                    $ret[$num] .= $query;
            }
            $num++;
        }
        return $ret;
    }

}
