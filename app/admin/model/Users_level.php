<?php
/***********************************************************
 * 会员等级模型
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
class Users_level extends Common
{
    //列表
    public function tableData($post)
    {
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $tableWhere = $this->pcftableWhere($post);
        $list = Db::name('users_level')->field($tableWhere['field'])->where($tableWhere['where'])->order($tableWhere['order'])->paginate($limit);
        $data = $this->tableFormat($list->getCollection())->toArray();
        $result = ['code' => 0, 'msg' => 'ok','count' =>$list->total(),'data' => $data];
        return $result;
    }

    protected function pcftableWhere($post)
    {
        $where = [];
        if (isset($post['level_name']) && $post['level_name'] != "") {
            $where[] = ['level_name', 'like', '%' . $post['level_name'] . '%'];
        }
        $result['where'] = $where;
        $result['field'] = "*";
        $result['order'] = "id desc";
        return $result;
    }
}
