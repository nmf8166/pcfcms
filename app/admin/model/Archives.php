<?php
/***********************************************************
 * 文档主表模型
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\model;

use think\Model;
use think\facade\Db;
use think\facade\Session;
use think\facade\Cache;
use think\facade\Request;
class Archives extends Model
{
    // 初始化
    protected function initialize()
    {
        parent::initialize();
    }

    // 统计每个栏目文档数
    public function afterSave($aid, $post)
    {
        if (isset($post['aid']) && intval($post['aid']) > 0) {
            $opt = 'edit';
            Db::name('article_content')->where('aid', $aid)->update($post);
        } else {
            $opt = 'add';
            $post['aid'] = $aid;
            Db::name('article_content')->insert($post);
        }
    }

    //伪删除栏目下所有文档
    public function pseudo_del($typeidArr)
    {
        $where=[];
        $where[] = ['typeid','IN', $typeidArr];
        $where[] = ['is_del','=', 0];
        Db::name('archives')
            ->where($where)
            ->update([
                'is_del' => 1,
                'del_method' => 2,
                'update_time' => getTime(),
            ]);
        return true;
    }

}