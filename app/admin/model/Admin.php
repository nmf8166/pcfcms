<?php
/***********************************************************
 * 管理员模型
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Session;
use think\facade\Request;
class Admin extends Common
{

    //管理员列表
    public function tableData($post)
    {
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $tableWhere = $this->pcftableWhere($post);
        $list = Db::name('admin')->field($tableWhere['field'])->where($tableWhere['where'])->order($tableWhere['order'])->paginate($limit);
        $data = $this->tableFormat($list->getCollection());
        $newdata = array();
        foreach ($data as $key => $value) {
            $newdata[$key]['admin_id'] = $value['admin_id'];
            $newdata[$key]['user_name'] = $value['user_name'];
            $newdata[$key]['pen_name'] = $value['pen_name'];
            $newdata[$key]['mobile'] = $value['mobile'];
            $newdata[$key]['email'] = $value['email'];
            $newdata[$key]['head_pic'] = $value['head_pic'];
            $newdata[$key]['sex'] = $value['sex'];
            $admin_name = Db::name('auth_role')->where('id', $value['role_id'])->value('name');
            $newdata[$key]['rolename'] = $admin_name ? $admin_name:'超级管理员';
            $newdata[$key]['status'] = $value['status'];
            $newdata[$key]['add_time'] = pcftime($value['add_time']);
            if($value['admin_id'] == 1 || Session::get('admin_id') == $value['admin_id'] || empty($value['parent_id'])){
                $newdata[$key]['nodel'] = true;
            }else{
                $newdata[$key]['nodel'] = false;
            }
        }
        $result = ['code' => 0, 'msg' => 'ok','count' =>$list->total(),'data' => $newdata];
        return $result;
    }

    protected function pcftableWhere($post)
    {
        $where = [];
        if (isset($post['user_name']) && $post['user_name'] != "") {
            $where[] = ['user_name', 'like', '%' . $post['user_name'] . '%'];
        }
        if (isset($post['mobile']) && $post['mobile'] != "") {
            $where[] = ['mobile', '=', $post['mobile'] ];
        }
        //权限控制
        $admin_info = Session::get('admin_info');
        if (0 < intval($admin_info['role_id'])) {
                $where[] = ['admin_id', '=', $admin_info['admin_id']];
                $where[] = ['parent_id', '=', $admin_info['parent_id']];
        } else {
            if (!empty($admin_info['parent_id'])) {
                $where[] = ['admin_id', '=', $admin_info['admin_id']];
                $where[] = ['parent_id', '=', $admin_info['parent_id']];
            }
        }
        $result['where'] = $where;
        $result['field'] = "*";
        $result['order'] = "admin_id desc";
        return $result;
    }
    
    //添加|编辑
    public function toAdd($data)
    {
        $result = array('status' => false,'data' => '','msg' => '');
        //判断是新增还是修改
        if (isset($data['admin_id']) && !empty($data['admin_id'])) {
            $edit_data = array();
            $manageInfo = Db::name('admin')->where('admin_id', $data['admin_id'])->find();
            if (!$manageInfo) {
                $result = ['status' => false, 'msg' => '账号不存在'];
                return $result;
            }
            if (isset($data['password']) && !empty($data['password'])) {
                if (strlen($data['password']) <= 5 || strlen($data['password']) > 16) {
                    $result = ['status' => false, 'msg' => '输入大于6位且小于16位密码'];
                    return $result;
                } else {
                    $data['password'] = func_encrypt($data['password']);
                    $edit_data['password'] = $data['password'];
                }
            } else {
                unset($data['password']);//不修改密码
            }
            $edit_data['user_name'] = isset($data['user_name']) ? $data['user_name']:'';
            $edit_data['pen_name'] = isset($data['pen_name']) ? $data['pen_name']:'';
            $edit_data['role_id']  = isset($data['role_id']) ? $data['role_id']:'';
            $edit_data['mobile'] = isset($data['mobile']) ? $data['mobile']:'';
            $edit_data['email'] = isset($data['email']) ? $data['email']:''; 
            $edit_data['sex'] = isset($data['sex']) ? $data['sex']:'';
            $edit_data['update_time'] = time();
            if (Db::name('admin')->where('admin_id', $data['admin_id'])->data($edit_data)->update()) {
                $result = ['status' => true, 'msg' => '修改成功','url' => url('/system.Admin/index')->suffix(false)->domain(true)->build()];
                return $result;
            } else {
                $result = ['status' => false, 'msg' => '修改失败'];
                return $result;
            }
        } else {
            $add_data = array();
            // 如果不是超级管理员不能添加管理员
            if (0 < intval(Session::get('admin_info.role_id'))) {
                $result = ['status' => false, 'msg' => '超级管理员才能操作！'];
                return $result;
            }
            if (empty($data['user_name'])) {
                $result = ['status' => false, 'msg' => '请输入账号！'];
                return $result;
            }
            if (empty($data['password']) || empty($data['password2'])) {
                $result = ['status' => false, 'msg' => '密码不能为空！'];
                return $result;
            }else if ($data['password'] != $data['password2']) {
                $result = ['status' => false, 'msg' => '两次密码输入不一致！'];
                return $result;              
            }else if (strlen($data['password']) <= 5 || strlen($data['password']) > 16 || strlen($data['password2']) <= 5 || strlen($data['password2']) > 16) {
                $result = ['status' => false, 'msg' => '输入大于6位且小于16位密码'];
                return $result;  
            }
            $manageInfo = Db::name('admin')->where(['user_name' => $data['user_name']])->find();
            if (!$data['user_name']) {
                $result = ['status' => false, 'msg' => '输入账号'];
                return $result;  
            }
            if ($manageInfo) {
                $result = ['status' => false, 'msg' => '账号已经存在'];
                return $result;
            }
            if (!isset($data['password'][5]) || isset($data['password'][16])) {
                $result = ['status' => false, 'msg' => '输入大于6位且小于16位密码'];
                return $result;
            }
            $add_data['parent_id'] = session::get('admin_info.admin_id');
            $add_data['user_name'] = isset($data['user_name']) ? $data['user_name']:'';
            $add_data['password'] = func_encrypt($data['password']);
            $add_data['pen_name'] = isset($data['pen_name']) ? $data['pen_name']:'';
            $add_data['role_id']  = isset($data['role_id']) ? $data['role_id']:'';
            $add_data['mobile'] = isset($data['mobile']) ? $data['mobile']:'';
            $add_data['email'] = isset($data['email']) ? $data['email']:'';          
            $add_data['sex'] = isset($data['sex']) ? $data['sex']:'';
            $add_data['add_time'] = time();
            if (Db::name('admin')->save($add_data)) {
                $result = ['status' => true, 'msg' => '添加成功','url' => url('/system.Admin/index')->suffix(false)->domain(true)->build()];
                return $result;
            } else {
                $result = ['status' => false, 'msg' => '添加失败'];
                return $result;
            }
        }
    }

}
