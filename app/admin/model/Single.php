<?php
/***********************************************************
 * 单页模型
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Session;
use think\facade\Cache;
use think\facade\Request;
use app\admin\logic\FieldLogic;
class Single 
{
    // 初始化
    protected function initialize()
    {
        parent::initialize();
    }
    
    /**
     * 后置操作方法
     * 自定义的一个函数 用于数据保存后做的相应处理操作, 使用时手动调用
     * @param int $aid 产品id
     * @param array $post post数据
     * @param string $opt 操作
     */
    public function afterSave($aid, $post, $opt)
    {
        $fieldLogic = new FieldLogic();
        $post['aid'] = $aid;
        $addonFieldExt = !empty($post['addonFieldExt']) ? $post['addonFieldExt'] : array();
        $fieldLogic->dealChannelPostData(6, $post, $addonFieldExt);
    }

    /**
     * 删除的后置操作方法
     * 自定义的一个函数 用于数据删除后做的相应处理操作, 使用时手动调用
     * @param int $aid
     */
    public function afterDel($typeidArr = array())
    {
        $where = [];
        if (is_string($typeidArr)) {
            $typeidArr = explode(',', $typeidArr);
        }
        $where[]=['typeid','IN', $typeidArr];
        // 同时删除单页文档表
        Db::name('archives')->where($where)->delete();
        // 同时删除内容表
        Db::name('single_content')->where($where)->delete();
        // 清除缓存
        Cache::delete("arctype");
        Cache::delete('admin_archives_release');
    }
}