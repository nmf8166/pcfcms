<?php
/***********************************************************
 * 登录日志
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller\system;
use app\admin\controller\Base;
use app\admin\model\AdminLog as LogModel;
use think\facade\Request;
use think\facade\Db;
class Adminlog extends Base
{

    public $popedom = '';
    public function initialize() {
        parent::initialize();
        $ctl_act = 'system.Admin/index';
        $this->popedom = appfile_popedom($ctl_act);
    }

    public function index(){
        if (Request::isAjax()) {
            $LogModel = new LogModel();
            return $LogModel->tableData(input('param.'));
        }
        return $this->fetch();  
    }

    //清除日志缓存并删出log空目录
    public function clear_log_chache()
    {
        //验证权限
        if(!$this->popedom["dellog"]){
            if(config('params.auth_msg.test')){
                $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                return $result;
            }else{
                $result = ['status' => false, 'msg' => config('params.auth_msg.delete')];
                return $result;                    
            }
        }
        $path = glob(ROOT_PATH.'runtime/log/*');
        foreach ($path as $item) {
            array_map('unlink', glob($item));
        }
        $result = ['status' => true, 'msg' => '清除日志成功'];
        return $result;
    }

}
