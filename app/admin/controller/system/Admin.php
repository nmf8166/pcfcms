<?php
/***********************************************************
 * 管理员
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller\system;
use think\facade\Db;
use think\facade\Request;
use think\facade\Session;
use app\admin\controller\Base;
use app\admin\model\Admin as AdminModel;
class Admin extends Base
{
    public $popedom = '';
    public function initialize() {
        parent::initialize();
        $ctl_act = Request::controller().'/index';
        $this->popedom = appfile_popedom($ctl_act);
    }
    //管理员列表
    public function index(){
        //验证查看权限
        if(!$this->popedom["list"]){
            return $this->errorNotice(config('params.auth_msg.list'),true,3,false);
        }
        if (Request::isAjax()) {
            $AdminModel = new AdminModel();
            return $AdminModel->tableData(input('param.'));
        }
        $list = Db::name('auth_role')->select()->toArray(); 
        $this->assign('list',$list);
        return $this->fetch();
    }
    //添加管理员
    public function add(){
        $AdminModel = new AdminModel();
        if (Request::isPost()) {
            //验证权限
            if(!$this->popedom["add"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.add')];
                    return $result;                    
                }
            }
            return $AdminModel->toAdd(input('param.'));
        }
        $manageRoleList = Db::name('auth_role')->select()->toArray();
        $this->assign('roleList', $manageRoleList);
        //获取当前管理员
        $admininfo = Db::name('admin')->field('role_id')->where('admin_id', session::get('admin_info.admin_id'))->find();
        $this->assign('admininfo',$admininfo);
        return $this->fetch('add');
    }
    //编辑管理员
    public function edit(){
        $AdminModel = new AdminModel();
        if (Request::isPost()) {
            //验证权限
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                } 
            } 
            return $AdminModel->toAdd(input('param.'));
        }
        $manageInfo = Db::name('admin')->where(['admin_id' => input('get.id/d')])->find();//管理员
        $manageRoleList = Db::name('auth_role')->select()->toArray();//管理组
        foreach ($manageRoleList as $k => $v) {
            if ($manageInfo['role_id'] == $v['id']) {
               $manageRoleList[$k]['checked'] = true;
            }else{
               $manageRoleList[$k]['checked'] = false;
            }
        }
        $admin_info = Session::get('admin_info');
        $this->assign('sess_admin_info', $admin_info);
        $this->assign('roleList', $manageRoleList);
        $this->assign('manageInfo', $manageInfo);
        return $this->fetch('edit');
    }

    //修改管理员状态
    public function updateState(){
        if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["status"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.status')];
                    return $result;                    
                }
            } 
            $admin_id = input('get.admin_id/d');
            if($admin_id == 1 || Session::get('admin_id') == $admin_id){
                $result = ['status' => false, 'msg' => '不能修改自己'];
                return $result;
            }
            $state  = input('get.state/d');
            if ($state == 0) {
                $state = 1;
            } else {
                $state = 0;
            }
            if (Db::name('admin')->where("admin_id = ".$admin_id)->save(array('status'=>$state))) {
                $result = ['status' => true, 'msg' => '设置成功'];
            } else {
                $result = ['status' => false, 'msg' => '设置失败'];
            }
            return $result;
        }
    }
    //删除用户
    public function del(){
         if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            } 
            $admin_id = input('get.admin_id/d');
            //超级管理员不能删除
            if($admin_id == 1){
                $result = ['status' => false, 'msg' => '超级管理员不能删除'];
                return $result;
            }
            //不能删除自己
            if($admin_id == Session::get('admin_id')){
                $result = ['status' => false, 'msg' => '不能删除自己'];
                return $result;
            }
            if (Db::name('admin')->where("admin_id",$admin_id)->delete()) {
                //登陆记录也删除
                Db::name('admin_log')->where("admin_id",$admin_id)->delete();
                $result = ['status' => true, 'msg' => '删除成功'];
            } else {
                $result = ['status' => false, 'msg' => '删除失败'];
            }
            return $result;
        }       
    }
    //重置密码
    public function restPsw(){
         if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            }
            $admin_id = input('get.admin_id/d');
            $manageInfo = Db::name('admin')->where('admin_id', $admin_id)->find();
            $newpassword = func_encrypt('PCFCMS123456');
            if($manageInfo['password'] == $newpassword){
                $result = ['status' => true, 'msg' => '密码已经是【PCFCMS123456】'];
                return $result;
            }
            if (Db::name('admin')->where('admin_id', $admin_id)->data(['password' => $newpassword])->update()) {
                $result = ['status' => true, 'msg' => '密码已经是【PCFCMS123456】'];
            } else {
                $result = ['status' => false, 'msg' => '重置失败'];
            }
            return $result;
        }         
    }
    //个人中心
    public function userinfo(){
        $AdminModel = new AdminModel();
        if (Request::isPost()) {
            if(!$this->popedom["userinfo"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.userinfo')];
                    return $result;                    
                }
            }
            return $AdminModel->toAdd(input('param.'));
        }
        $admin_id = Session::get('admin_id');
        $manageInfo = Db::name('admin')->where('admin_id' , $admin_id)->find();
        if (!$manageInfo) {
            $result = ['status' => false, 'msg' => '失败'];
            return $result;
        }
        $this->assign('manageInfo', $manageInfo);
        return $this->fetch();
    }

    // 文档内容里面设置作者默认名称 
    public function ajax_setfield()
    {
        if (Request::isAjax()) {
            $admin_id = session::get('admin_id');
            $field  = input('field'); // 修改哪个字段
            $value  = input('value', '', null); // 修改字段值  
            if (!empty($admin_id)) {
                $r = Db::name('admin')->where('admin_id',intval($admin_id))->save([$field=>$value,'update_time'=>getTime()]);
                if ($r) {
                    // 更新存储在session里的信息
                    $admin_info = session::get('admin_info');
                    $admin_info[$field] = $value;
                    session::set('admin_info', $admin_info);
                    $result = ['status' => true, 'msg' => '操作成功'];
                    return $result;
                }
            }
        }
        $result = ['status' => false, 'msg' => '操作失败'];
        return $result;
    }

    // 编辑默认栏目
    public function ajax_settypeid(){
        $typeid = input('typeid/d',0);
        if ($typeid){
            $admin_info = session::get('admin_info');
            $r = Db::name('admin')->where("admin_id = ".$admin_info['admin_id'])->save(array('update_time'=>time(),'typeid'=>$typeid));
            if ($r){
                $admin_info['typeid'] = $typeid;
                session::set('admin_info', $admin_info);
                $result = ['status' => true, 'msg' => '操作成功'];
                return $result;
            }
        }
        $result = ['status' => false, 'msg' => '操作失败'];
        return $result;
    }

}
