<?php
/**
 * 文档模型
 * ============================================================================
 * 网站地址: http://www.pcfcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小潘 <1131680521@qq.com>
 * Date: 2019-12-21
 */
namespace app\home\model;

use think\Model;
use think\facade\Db;
use app\home\logic\FieldLogic;
use app\home\model\Article;
use app\home\model\Single;
// 文档主表
class Archives extends Model
{
    // 初始化
    protected function initialize()
    {
        parent::initialize();
        $this->fieldLogic = new FieldLogic();
    }

    // 获取单条文档记录
    public function getViewInfo($aid, $litpic_remote = false)
    {
        $result = array();
        $row = Db::name('archives')->field('*')->find($aid);
        if (!empty($row)) {
            // 封面图
            if (empty($row['litpic'])) {
                $row['is_litpic'] = 0; // 无封面图
            } else {
                $row['is_litpic'] = 1; // 有封面图
            }
            $row['litpic'] = get_default_pic($row['litpic'], $litpic_remote); // 默认封面图
            // 文档基本信息
            if (1 == $row['channel']) { // 文章模型
                $articleModel = new Article();
                $rowExt = $articleModel->getInfo($aid);
            }
            $rowExt = $this->fieldLogic->getChannelFieldList($rowExt, $row['channel']); // 自定义字段的数据格式处理
            $result = array_merge($rowExt, $row);
        }
        return $result;
    }

    // 获取单页栏目记录
    public function getSingleInfo($typeid, $litpic_remote = false)
    {
        $result = array();
        // 文档基本信息
        $singleModel = new Single();
        $row = $singleModel->getInfoByTypeid($typeid);
        if (!empty($row)) {
            // 封面图
            if (empty($row['litpic'])) {
                $row['is_litpic'] = 0; // 无封面图
            } else {
                $row['is_litpic'] = 1; // 有封面图
            }
            $row['litpic'] = get_default_pic($row['litpic'], $litpic_remote); // 默认封面图
            $row = $this->fieldLogic->getTableFieldList($row, config('global.arctype_channel_id')); // 自定义字段的数据格式处理
            $row = $this->fieldLogic->getChannelFieldList($row, $row['channel']); // 自定义字段的数据格式处理
            $result = $row;
        }
        return $result;
    }
    
}