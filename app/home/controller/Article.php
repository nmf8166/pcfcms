<?php
/***********************************************************
 * 文章模型 系统自带
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\home\controller;
use think\facade\Db;
use app\common\model\Arctype;

class Article extends Base
{
    // 模型标识
    public $nid = 'article';
    // 模型ID
    public $channeltype = '';
    
    public function initialize() {
        parent::initialize();
        $channeltype_list = config('global.channeltype_list');
        $this->diymodel = new \app\home\model\Article();
        $this->channeltype = $channeltype_list[$this->nid];
    }

    public function lists($tid)
    {
        $tid_tmp = $tid;
        $seo_pseudo = tpCache('seo.seo_pseudo');
        if (empty($tid)) {
            $map = array(
                'channeltype'   => $this->channeltype,
                'parent_id' => 0,
                'is_hidden' => 0,
                'status'    => 1,
            );
        } else {
            if (3 == $seo_pseudo) {
                $map = array('dirname'=>$tid);
            } else {
                if (!is_numeric($tid) || strval(intval($tid)) !== strval($tid)) {
                    abort(404,'页面不存在');
                }
                $map = array('id'=>$tid);
            }
        }
        $row = Db::name('arctype')->field('id,dirname')->where($map)->order('sort_order asc')->limit(1)->find();
        $tid = !empty($row['id']) ? intval($row['id']) : 0;
        $dirname = !empty($row['dirname']) ? $row['dirname'] : '';
        if (3 == $seo_pseudo) {
            $tid = $dirname;
        } else {
            $tid = $tid_tmp;
        }
        $pcflist = new \app\home\controller\Lists($this->app);
        return $pcflist->index($tid);
    }

    public function view($aid)
    {
        $result = $this->diymodel->getInfo($aid);
        if (empty($result)) {
            abort(404,'页面不存在');
        } elseif ($result['arcrank'] == -1) {
            return '待审核稿件，你没有权限阅读！';
        }
        // 外部链接跳转
        if ($result['is_jump'] == 1) {
            header('Location: '.$result['jumplinks']);
        }
        $pcflist = new \app\home\controller\View($this->app);
        return $pcflist->index($aid);
    }

}