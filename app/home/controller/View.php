<?php
/***********************************************************
 * 详细内容
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\home\controller;
use think\facade\Db;
use think\facade\Request;
use think\facade\Session;
use app\common\model\Arctype;
use app\home\model\Article;
use app\home\model\Single;
use app\home\model\Custom;
class View extends Base
{
    // 模型标识
    public $nid = '';
    // 模型ID
    public $channel = '';
    // 模型名称
    public $modelName = '';

    public function initialize() {
        parent::initialize();
        $this->Arctypemodel = new Arctype();
    }

    public function index($aid = '')
    {
        $seo_pseudo = tpCache('seo.seo_pseudo');
        $seo_rewrite_format = tpCache('seo.seo_rewrite_format');
        if (!is_numeric($aid) || strval(intval($aid)) !== strval($aid)) {
            abort(404,'页面不存在');
        }
        // URL上参数的校验
        if (3 == $seo_pseudo){
            if (stristr($this->request->url(), 'View/index')) {
                abort(404,'页面不存在');
            }
        }else if (1 == $seo_pseudo || (2 == $seo_pseudo && 'wap' == common_ismobile())){
            $seo_dynamic_format = tpCache('seo.seo_dynamic_format');
            if (1 == $seo_pseudo && 2 == $seo_dynamic_format && stristr($this->request->url(), 'View/index')) {
                abort(404,'页面不存在');
            }
        }
        $aid = intval($aid);
        $archivesInfo = Db::name('archives')->field('a.typeid, a.channel, b.nid, b.ctl_name')
            ->alias('a')
            ->join('channel_type b', 'a.channel = b.id', 'LEFT')
            ->where([
                'a.aid' => $aid,
                'a.is_del' => 0,
            ])
            ->find();
        if (empty($archivesInfo) || !in_array($archivesInfo['channel'], $this->pcfglobal['allow_release_channel'])) {
            abort(404,'页面不存在');
        }
        $this->nid = $archivesInfo['nid'];
        $this->channel = $archivesInfo['channel'];
        $this->modelName = $archivesInfo['ctl_name'];
        switch ($this->channel) {
            // 单页模型
            case '1': 
            {
                $diymodel = new Article();
                $result = $diymodel->getInfo($aid);
                break;
            }
            // 文章模型
            case '6': 
            {
                $diymodel = new Single();
                $result = $diymodel->getInfo($aid);
                break;
            }
            // 自定义模型
            default:
            {
                $diymodel = new Custom();
                $result = $diymodel->getInfo($aid);
                break;
            }
        }
        // 若是管理员则不受限制
        if (Session::get('admin_id') ) {
            if ($result['status'] == 0) {
                abort(404,'待审核稿件，你没有权限阅读！');
            }
        }
        // 外部链接跳转
        if ($result['is_jump'] == 1) {
            header('Location: '.$result['jumplinks']);
        }
        $tid = $result['typeid'];
        $arctypeInfo = $this->Arctypemodel->getInfo($tid);
		
        // 自定义字段的数据格式处理
        $arctypeInfo = $this->fieldLogic->getTableFieldList($arctypeInfo, $this->pcfglobal['arctype_channel_id']);
        if (!empty($arctypeInfo)) {
            // URL上参数的校验
            if (3 == $seo_pseudo) {
                $dirname = input('param.dirname/s');
                $dirname2 = '';
                if (1 == $seo_rewrite_format) {
                    $toptypeRow = $this->Arctypemodel->getAllPid($tid);
                    $toptypeinfo = current($toptypeRow);
                    $dirname2 = $toptypeinfo['dirname'];
                } else if (2 == $seo_rewrite_format) {
                    $dirname2 = $arctypeInfo['dirname'];
                }
                /*if ($dirname != $dirname2) {
                    abort(404,'页面不存在');
                }*/
            }
            // 是否有子栏目，用于标记【全部】选中状态
            $arctypeInfo['has_children'] = $this->Arctypemodel->hasChildren($tid);
            // 文档模板文件，不指定文档模板，默认以栏目设置的为主
            empty($result['tempview']) && $result['tempview'] = $arctypeInfo['tempview'];
            // 给没有type前缀的字段新增一个带前缀的字段，并赋予相同的值
            foreach ($arctypeInfo as $key => $val) {
                if (!preg_match('/^type/i',$key)) {
                    $key_new = 'type'.$key;
                    !array_key_exists($key_new, $arctypeInfo) && $arctypeInfo[$key_new] = $val;
                }
            }
        } else {
            abort(404,'页面不存在');
        }
        $result = array_merge($arctypeInfo, $result);
        // 文档链接
        $result['arcurl'] = $result['pageurl'] = '';
        if ($result['is_jump'] != 1) {
            $result['arcurl'] = $result['pageurl'] = $this->request->url(true);
        }
        // seo标题
        $result['seo_title'] = set_arcseotitle($result['title'], $result['seo_title'], $result['typename']);
        $result['seo_description'] = @pcfmsubstr(checkStrHtml($result['seo_description']), 0, $this->pcfglobal['arc_seo_description_length'], false);
        // 支持子目录
        $result['litpic'] = handle_subdir_pic($result['litpic']);
        $result = view_logic($aid, $this->channel, $result, true); // 模型对应逻辑
        // 自定义字段的数据格式处理
        $result = $this->fieldLogic->getChannelFieldList($result, $this->channel);
        $gzpcf = array('type' => $arctypeInfo,'field' => $result,);
        $this->gzpcf = array_merge($this->gzpcf, $gzpcf);
        $this->assign('gzpcf', $this->gzpcf);
        // 模板文件
        $viewfile = !empty($result['tempview']) ? str_replace('.'.$this->view_suffix, '',$result['tempview']) : 'view_'.$this->nid;
        return $this->fetch(":{$viewfile}");
    }

}