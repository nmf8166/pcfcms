<?php
/***********************************************************
 * 首页
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\home\controller;
use think\facade\Db;
use think\facade\Request;
use think\facade\Session;
class Index extends Base
{
    public function initialize() {
        parent::initialize();
    }

    public function index(){
        $filename = 'index.html';
        // 生成静态页面代码 - PC端动态访问跳转到静态
        $seo_pseudo = tpCache('seo.seo_pseudo');
        if (file_exists($filename) && 2 == $seo_pseudo) {
            if ((common_ismobile()== "wap" && !file_exists(ROOT_PATH."template/{$this->tpl_theme}/mobile"))) {
                header('HTTP/1.1 301 Moved Permanently');
                header('Location:index.html');
            }
        }
        // 获取当前页面URL
        $result['pageurl'] = 'index';
        $gzpcf = array('field' => $result);
        $this->gzpcf = array_merge($this->gzpcf, $gzpcf);
        $this->assign('gzpcf', $this->gzpcf);
        return $this->fetch(":index");
    }

}